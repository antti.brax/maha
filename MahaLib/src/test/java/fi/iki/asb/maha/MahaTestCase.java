package fi.iki.asb.maha;

import java.util.Iterator;
import java.util.LinkedList;

import junit.framework.TestCase;

import com.google.common.eventbus.DeadEvent;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import fi.iki.asb.maha.event.ColorSelectionUpdatedEvent;
import fi.iki.asb.maha.event.GameOverEvent;
import fi.iki.asb.maha.event.ModelClearedEvent;
import fi.iki.asb.maha.event.ModelUpdatedEvent;
import fi.iki.asb.maha.event.PointsScoredEvent;
import fi.iki.asb.maha.event.ScoredUpdatedEvent;
import fi.iki.asb.maha.event.StartNewGameEvent;
import fi.iki.asb.maha.event.StatisticsUpdatedEvent;
import fi.iki.asb.maha.logic.GameOverDetector;
import fi.iki.asb.maha.logic.PersistenceHandler;
import fi.iki.asb.maha.logic.ModelTouchHandler;
import fi.iki.asb.maha.logic.ModelFiller;
import fi.iki.asb.maha.logic.PointsCalculator;
import fi.iki.asb.maha.logic.Statistician;
import fi.iki.asb.maha.logic.Tally;
import fi.iki.asb.maha.model.Color;
import fi.iki.asb.maha.model.Dimension;
import fi.iki.asb.maha.model.Model;
import fi.iki.asb.maha.ui.TouchConverter;
import fi.iki.asb.maha.util.RandomIterator;

public abstract class MahaTestCase extends TestCase {

    protected EventBus eventBus;

    protected ColorSelectionUpdatedEvent colorSelectionUpdatedEvent;
    protected GameOverEvent gameOverEvent;
    protected ModelClearedEvent modelClearedEvent;
    protected final LinkedList<ModelUpdatedEvent> modelUpdatedEvents = new LinkedList<ModelUpdatedEvent>();
    protected final LinkedList<PointsScoredEvent> pointsScoredEvents = new LinkedList<PointsScoredEvent>();
    protected final LinkedList<ScoredUpdatedEvent> scoreUpdatedEvents = new LinkedList<ScoredUpdatedEvent>();
    protected final LinkedList<StatisticsUpdatedEvent> statisticsUpdatedEvents = new LinkedList<StatisticsUpdatedEvent>();

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        eventBus = createEventBus();
        eventBus.register(this);
    }

    protected EventBus createEventBus() {
        return new EventBus();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        eventBus.unregister(this);
        eventBus = null;

        colorSelectionUpdatedEvent = null;
        gameOverEvent = null;
        modelClearedEvent = null;
        modelUpdatedEvents.clear();
        pointsScoredEvents.clear();
        scoreUpdatedEvents.clear();
        statisticsUpdatedEvents.clear();
    }

    protected Model lastModel() {
        return modelUpdatedEvents.getLast().getModel();
    }

    @Subscribe
    public void onColorSelectionUpdated(ColorSelectionUpdatedEvent event) {
        colorSelectionUpdatedEvent = event;
    }

    @Subscribe
    public void onGameOver(GameOverEvent event) {
        gameOverEvent = event;
    }

    @Subscribe
    public void onModelCleared(ModelClearedEvent event) {
        modelClearedEvent = event;
    }

    @Subscribe
    public void onModelUpdated(ModelUpdatedEvent event) {
        modelUpdatedEvents.add(event);
    }

    @Subscribe
    public void onPointsScored(PointsScoredEvent event) {
        pointsScoredEvents.add(event);
    }

    @Subscribe
    public void onScoredUpdated(ScoredUpdatedEvent event) {
        scoreUpdatedEvents.add(event);
    }

    @Subscribe
    public void onStatisticsUpdated(StatisticsUpdatedEvent event) {
        statisticsUpdatedEvents.add(event);
    }

    @Subscribe
    public void onDeadEvent(DeadEvent event) {
        fail(event.toString());
    }

    protected void init(int width, int height,
                        Iterator<Color> colorProvider) {
        new GameOverDetector(eventBus);
        new ModelFiller(eventBus, colorProvider);
        new ModelTouchHandler(eventBus);
        new PersistenceHandler(eventBus);
        new TouchConverter(eventBus);
        new PointsCalculator(eventBus);
        new Tally(eventBus);
        new Statistician(eventBus);

        eventBus.post(new StartNewGameEvent(new Dimension(width, height)));
    }

    protected void init(int width, int height) {
        init(width, height, new RandomIterator(5));
    }
}
