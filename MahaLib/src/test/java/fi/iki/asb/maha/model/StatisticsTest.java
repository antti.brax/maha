package fi.iki.asb.maha.model;

import org.apache.commons.codec.binary.Base64;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

import fi.iki.asb.maha.MahaTestCase;
import fi.iki.asb.maha.StaticIterator;
import fi.iki.asb.maha.event.StatisticsUpdatedEvent;
import fi.iki.asb.maha.event.ViewportTouchEvent;

public class StatisticsTest extends MahaTestCase {

    public void testViewportTouch() {
        init(2, 2, new StaticIterator(Color.valueOf(1)));

        assertEquals(1, statisticsUpdatedEvents.size());
        assertEquals(1, statisticsUpdatedEvents.getFirst().getStatistics().getGamesStarted());
        assertEquals(0, statisticsUpdatedEvents.getFirst().getStatistics().getViewportTouched());

        eventBus.post(new ViewportTouchEvent(5, 0));

        assertEquals(2, statisticsUpdatedEvents.size());
        assertEquals(1, statisticsUpdatedEvents.getLast().getStatistics().getGamesStarted());
        assertEquals(1, statisticsUpdatedEvents.getLast().getStatistics().getViewportTouched());
    }

    // =================================================================== //
    // Regression tests for reading old persistence versions.

    private static final String STATISTICS_V1 = "rO0ABXcxAQAAAAAAAAAPAAAAAAAAAAEAAAAAAAAAewAAAAAAAAAOAAAAAAAAAEIAAAAAAAAAAQ==";

    public void testWriteLatest() throws IOException {
        Statistics stats = new Statistics()
                .addBlocksRemoved(15)
                .addGamesStarted()
                .setHighScore(123)
                .setLowScore(14)
                .addPointsScored(66)
                .addViewportTouched();

        ByteArrayOutputStream bo = new ByteArrayOutputStream(64);
        ObjectOutputStream oo = new ObjectOutputStream(bo);
        Statistics.write(stats, oo);
        oo.flush();

        assertEquals(STATISTICS_V1, Base64.encodeBase64String(bo.toByteArray()));
    }

    public void testRead_V1() throws IOException {
        ByteArrayInputStream bi = new ByteArrayInputStream(Base64.decodeBase64(STATISTICS_V1));
        ObjectInputStream oi = new ObjectInputStream(bi);
        Statistics stats = Statistics.read(oi);

        assertEquals(15, stats.getBlocksRemoved());
        assertEquals(1, stats.getGamesStarted());
        assertEquals(123, stats.getHighScore());
        assertEquals(14, stats.getLowScore());
        assertEquals(66, stats.getPointsScored());
        assertEquals(1, stats.getViewportTouched());
    }

}
