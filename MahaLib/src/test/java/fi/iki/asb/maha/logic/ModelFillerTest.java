package fi.iki.asb.maha.logic;

import fi.iki.asb.maha.MahaTestCase;
import fi.iki.asb.maha.model.Location;
import fi.iki.asb.maha.model.Model;

public class ModelFillerTest extends MahaTestCase {

    public void testStartNewGame() {
        init(4, 4);

        assertEquals(1, modelUpdatedEvents.size());
        assertEquals(16, modelUpdatedEvents.getLast().getLocations().size());

        final Model model = lastModel();
        for (Location l: model.getDimensions().forEach())
            assertNotNull(model.getColorAt(l));
    }

}
