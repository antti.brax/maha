package fi.iki.asb.maha.model;

import java.util.Iterator;
import java.util.NoSuchElementException;

import junit.framework.TestCase;

public class LocationTest extends TestCase {

    public void testForEachInvalidRange() {
        try {
            Location from = Location.valueOf(4, 4);
            Location to = Location.valueOf(2, 2);
            Location.forEach(from, to).iterator();
            fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException ex) {
            // Ok.
        }

        try {
            Location from = Location.valueOf(2, 2);
            Location to = Location.valueOf(2, 2);
            Location.forEach(from, to).iterator();
            fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException ex) {
            // Ok.
        }
    }

    public void testForEach1() {
        Location from = Location.valueOf(2, 2);
        Location to = Location.valueOf(4, 4);

        Iterator<Location> iter = Location.forEach(from, to).iterator();
        assertTrue(iter.hasNext());
        assertEquals(Location.valueOf(2, 2), iter.next());
        assertTrue(iter.hasNext());
        assertEquals(Location.valueOf(3, 2), iter.next());
        assertTrue(iter.hasNext());
        assertEquals(Location.valueOf(2, 3), iter.next());
        assertTrue(iter.hasNext());
        assertEquals(Location.valueOf(3, 3), iter.next());
        assertFalse(iter.hasNext());
        try {
            iter.next();
            fail("Expected NoSuchElementException");
        } catch (NoSuchElementException ex) {
            // Ok.
        }
    }

    public void testForEach2() {
        Location from = Location.valueOf(2, 2);
        Location to = Location.valueOf(3, 3);

        Iterator<Location> iter = Location.forEach(from, to).iterator();
        assertTrue(iter.hasNext());
        assertEquals(Location.valueOf(2, 2), iter.next());
        assertFalse(iter.hasNext());
        try {
            iter.next();
            fail("Expected NoSuchElementException");
        } catch (NoSuchElementException ex) {
            // Ok.
        }
    }

}
