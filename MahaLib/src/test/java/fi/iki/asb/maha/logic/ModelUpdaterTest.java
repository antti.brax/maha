package fi.iki.asb.maha.logic;

import java.util.Collection;

import fi.iki.asb.maha.MahaTestCase;
import fi.iki.asb.maha.StaticIterator;
import fi.iki.asb.maha.logic.ModelUpdater;
import fi.iki.asb.maha.model.Color;
import fi.iki.asb.maha.model.Location;
import fi.iki.asb.maha.model.UpdatedLocation;

public class ModelUpdaterTest extends MahaTestCase {

    public void testUpdater() {
        init(2, 2, new StaticIterator(Color.valueOf(0)));

        assertEquals(1, modelUpdatedEvents.size());

        ModelUpdater updater = new ModelUpdater(lastModel())
                .set(0, 1, Color.valueOf(1));

        assertEquals(1, modelUpdatedEvents.size());

        eventBus.post(updater.commit());

        assertEquals(2, modelUpdatedEvents.size());
        Collection<UpdatedLocation> locations = modelUpdatedEvents.getLast().getLocations();
        assertEquals(1, locations.size());
        UpdatedLocation location = locations.iterator().next();
        assertNull(location.getOldLocation());
        assertEquals(Location.valueOf(0, 1), location.getNewLocation());
    }
    
    public void testMove() {
        init(2, 2, new StaticIterator(Color.valueOf(0)));
        
        eventBus.post(new ModelUpdater(lastModel())
                .set(0, 0, Color.valueOf(1))
                .commit());

        eventBus.post(new ModelUpdater(lastModel())
                .move(0, 0, 1, 1)
                .commit());
        
        assertEquals(null, lastModel().getColorAt(0, 0));
        assertEquals(Color.valueOf(1), lastModel().getColorAt(1, 1));
    }

}
