package fi.iki.asb.maha.model;

import java.util.Iterator;
import java.util.NoSuchElementException;

import junit.framework.TestCase;

public class DimensionTest extends TestCase {

    public void testIndexing1() {
        Dimension dimension = new Dimension(3, 2);
        assertEquals(0, dimension.index(0, 0));
        assertEquals(1, dimension.index(1, 0));
        assertEquals(2, dimension.index(2, 0));
        assertEquals(3, dimension.index(0, 1));
        assertEquals(4, dimension.index(1, 1));
        assertEquals(5, dimension.index(2, 1));
        try {
            assertEquals(5, dimension.index(0, 2));
            fail("Expected ArrayIndexOutOfBoundsException");
        } catch (ArrayIndexOutOfBoundsException ex) {
            // Ok.
        }
        try {
            assertEquals(5, dimension.index(3, 0));
            fail("Expected ArrayIndexOutOfBoundsException");
        } catch (ArrayIndexOutOfBoundsException ex) {
            // Ok.
        }
    }

    public void testIndexing2() {
        Dimension dimension = new Dimension(2, 2);
        assertEquals(3, dimension.index(Location.valueOf(1, 1)));
        assertEquals(3, dimension.index(1, 1));
    }

    public void testForEach() {
        Dimension dimension = new Dimension(2, 2);

        Iterator<Location> iter = dimension.forEach().iterator();
        assertTrue(iter.hasNext());
        assertEquals(Location.valueOf(0, 0), iter.next());
        assertTrue(iter.hasNext());
        assertEquals(Location.valueOf(1, 0), iter.next());
        assertTrue(iter.hasNext());
        assertEquals(Location.valueOf(0, 1), iter.next());
        assertTrue(iter.hasNext());
        assertEquals(Location.valueOf(1, 1), iter.next());
        assertFalse(iter.hasNext());
        try {
            iter.next();
            fail("Expected NoSuchElementException");
        } catch (NoSuchElementException ex) {
            // Ok.
        }
    }
}
