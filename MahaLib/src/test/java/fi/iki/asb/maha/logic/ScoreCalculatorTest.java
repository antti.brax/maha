package fi.iki.asb.maha.logic;

import fi.iki.asb.maha.MahaTestCase;
import fi.iki.asb.maha.StaticIterator;
import fi.iki.asb.maha.event.ModelTouchEvent;
import fi.iki.asb.maha.event.PointsScoredEvent;
import fi.iki.asb.maha.model.Color;

public class ScoreCalculatorTest extends MahaTestCase {

    /**
     * Test that correct PointsScoredEvents are created when the player
     * manages to clear the model.
     */
    public void testClearModel() {
        init(2, 2, new StaticIterator(Color.valueOf(1)));

        eventBus.post(new ModelTouchEvent(lastModel(), 0, 0));
        eventBus.post(new ModelTouchEvent(lastModel(), 0, 0));

        for (PointsScoredEvent e: pointsScoredEvents)
            System.err.println(e);

        assertEquals(2, pointsScoredEvents.size());
        assertEquals(4, pointsScoredEvents.getFirst().getPoints());
        assertEquals(250, pointsScoredEvents.getLast().getPoints());
    }

    public void testRemove() {
        init(2, 2, new StaticIterator(Color.valueOf(1)));

        //
        // 1 0
        // 1 1
        //
        eventBus.post(new ModelUpdater(lastModel())
                .set(0, 1, Color.valueOf(0))
                .commit());

        eventBus.post(new ModelTouchEvent(lastModel(), 0, 0));
        eventBus.post(new ModelTouchEvent(lastModel(), 0, 0));
        assertEquals(1, pointsScoredEvents.size());
        assertEquals(1, pointsScoredEvents.getFirst().getPoints());
    }
}
