package fi.iki.asb.maha.model;

import org.apache.commons.codec.binary.Base64;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

import fi.iki.asb.maha.MahaTestCase;
import fi.iki.asb.maha.event.PointsScoredEvent;
import fi.iki.asb.maha.event.StartNewGameEvent;
import fi.iki.asb.maha.logic.ModelUpdater;

public class ScoreTest extends MahaTestCase {

    public void testInit() {
        init(2, 2);
        assertEquals(1, scoreUpdatedEvents.size());
        assertEquals(0, scoreUpdatedEvents.getLast().getScore().getPoints());
    }

    public void testTally() {
        init(2, 2);
        assertEquals(1, scoreUpdatedEvents.size());

        eventBus.post(new PointsScoredEvent(5, 0));
        eventBus.post(new PointsScoredEvent(4, 0));

        assertEquals(3, scoreUpdatedEvents.size());
        assertEquals(5, scoreUpdatedEvents.get(1).getScore().getPoints());
        assertEquals(9, scoreUpdatedEvents.get(2).getScore().getPoints());

        eventBus.post(new PointsScoredEvent(2, 0));

        assertEquals(4, scoreUpdatedEvents.size());
        assertEquals(11, scoreUpdatedEvents.getLast().getScore().getPoints());
    }

    public void testStartNewGame() {
        init(4, 4);
        assertEquals(1, scoreUpdatedEvents.size());

        eventBus.post(new PointsScoredEvent(555, 0));
        eventBus.post(new StartNewGameEvent(new Dimension(4, 4)));

        assertEquals(3, scoreUpdatedEvents.size());
        assertEquals(0, scoreUpdatedEvents.get(0).getScore().getPoints());
        assertEquals(555, scoreUpdatedEvents.get(1).getScore().getPoints());
        assertEquals(0, scoreUpdatedEvents.get(2).getScore().getPoints());
    }

    // =================================================================== //
    // Regression tests for reading old persistence versions.

    private static final String SCORE_V1 = "rO0ABXcJAQAAAAAAAABB";

    public void testWriteLatest() throws IOException {
        Score score = new Score().addPoints(65);

        ByteArrayOutputStream bo = new ByteArrayOutputStream(64);
        ObjectOutputStream oo = new ObjectOutputStream(bo);
        Score.write(score, oo);
        oo.flush();

        assertEquals(SCORE_V1, Base64.encodeBase64String(bo.toByteArray()));
    }

    public void testRead_V1() throws IOException {
        ByteArrayInputStream bi = new ByteArrayInputStream(Base64.decodeBase64(SCORE_V1));
        ObjectInputStream oi = new ObjectInputStream(bi);
        Score score = Score.read(oi);

        assertEquals(65, score.getPoints());
    }

}
