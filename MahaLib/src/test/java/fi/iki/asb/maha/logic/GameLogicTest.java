package fi.iki.asb.maha.logic;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

import fi.iki.asb.maha.MahaTestCase;
import fi.iki.asb.maha.event.ModelTouchEvent;
import fi.iki.asb.maha.model.Color;
import fi.iki.asb.maha.model.Location;
import fi.iki.asb.maha.model.UpdatedLocation;

public class GameLogicTest extends MahaTestCase {

    //
    // 1 0
    // 1 1
    //

    protected Iterator<Color> createColorIterator() {
        return Arrays.asList(
                Color.valueOf(1),
                Color.valueOf(0),
                Color.valueOf(1),
                Color.valueOf(1),
                Color.valueOf(1),
                Color.valueOf(0),
                Color.valueOf(1),
                Color.valueOf(1))
                .iterator();
    }

    public void testSelectFirst() {
        init(2, 2, createColorIterator());

        assertNull(colorSelectionUpdatedEvent);
        eventBus.post(new ModelTouchEvent(lastModel(), 0, 0));
        assertNotNull(colorSelectionUpdatedEvent);

        Collection<Location> locations = colorSelectionUpdatedEvent.getSelection().getLocations();
        assertTrue(locations.contains(Location.valueOf(0, 0)));
        assertTrue(locations.contains(Location.valueOf(0, 1)));
        assertTrue(locations.contains(Location.valueOf(1, 1)));
        assertEquals(3, locations.size());
    }

    public void testSelectDifferent() {
        init(2, 2, createColorIterator());

        assertNull(colorSelectionUpdatedEvent);
        eventBus.post(new ModelTouchEvent(lastModel(), 1, 0));
        assertNotNull(colorSelectionUpdatedEvent);

        Collection<Location> locations = colorSelectionUpdatedEvent.getSelection().getLocations();
        assertEquals(0, locations.size());
    }

    public void testSelectSame() {
        init(2, 2, createColorIterator());
        assertEquals(1, modelUpdatedEvents.size());

        eventBus.post(new ModelTouchEvent(lastModel(), 0, 0));
        eventBus.post(new ModelTouchEvent(lastModel(), 0, 1));

        assertEquals(2, modelUpdatedEvents.size());
        Collection<UpdatedLocation> locations = modelUpdatedEvents.getLast().getLocations();
        assertTrue(locations.contains(new UpdatedLocation(Location.valueOf(0, 0), null)));
        assertTrue(locations.contains(new UpdatedLocation(Location.valueOf(0, 1), null)));
        assertTrue(locations.contains(new UpdatedLocation(Location.valueOf(1, 1), null)));
        assertTrue(locations.contains(new UpdatedLocation(Location.valueOf(1, 0), Location.valueOf(0, 1))));
        assertEquals(4, locations.size());
    }
}
