package fi.iki.asb.maha.model;

import junit.framework.TestCase;

import org.apache.commons.codec.binary.Base64;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import fi.iki.asb.maha.logic.ModelUpdater;

public class ModelTest extends TestCase {

    // =================================================================== //
    // Regression tests for reading old persistence versions.

    private static final String MODEL_V1 = "rO0ABXcLAQEAAAADAAAAAwFzcgARamF2YS5sYW5nLkludGVnZXIS4qCk94GHOAIAAUkABXZhbHVleHIAEGphdmEubGFuZy5OdW1iZXKGrJUdC5TgiwIAAHhwAAAAAHcBAXB3AQFwdwEBcHcBAXNxAH4AAAAAAAF3AQFwdwEBcHcBAXB3AQFzcQB+AAAAAAAC";

    public void testWriteLatest() throws IOException {
        Model model = new ModelUpdater(new Model(new Dimension(3, 3)))
                .set(0, 0, Color.valueOf(0))
                .set(1, 1, Color.valueOf(1))
                .set(2, 2, Color.valueOf(2))
                .commit()
                .getModel();

        ByteArrayOutputStream bo = new ByteArrayOutputStream(64);
        ObjectOutputStream oo = new ObjectOutputStream(bo);
        Model.write(model, oo);
        oo.flush();

        assertEquals(MODEL_V1, Base64.encodeBase64String(bo.toByteArray()));
    }

    public void testRead_V1() throws IOException {
        ByteArrayInputStream bi = new ByteArrayInputStream(Base64.decodeBase64(MODEL_V1));
        ObjectInputStream oi = new ObjectInputStream(bi);
        Model model = Model.read(oi);

        assertEquals(new Dimension(3, 3), model.getDimensions());
        assertEquals(Color.valueOf(0), model.getColorAt(0, 0));
        assertEquals(Color.valueOf(1), model.getColorAt(1, 1));
        assertEquals(Color.valueOf(2), model.getColorAt(2, 2));
    }

}
