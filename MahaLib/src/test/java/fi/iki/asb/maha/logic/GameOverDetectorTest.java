package fi.iki.asb.maha.logic;

import fi.iki.asb.maha.MahaTestCase;
import fi.iki.asb.maha.StaticIterator;
import fi.iki.asb.maha.event.ModelTouchEvent;
import fi.iki.asb.maha.model.Color;

public class GameOverDetectorTest extends MahaTestCase {

    public void testStartNewGame() {
        init(3, 3, new StaticIterator(Color.valueOf(0)));
        eventBus.post(new ModelUpdater(lastModel())
            .set(0, 0, Color.valueOf(1))
            .set(1, 0, Color.valueOf(2))
            .set(2, 0, Color.valueOf(2))
            .commit());

        eventBus.post(new ModelTouchEvent(lastModel(), 1, 0));
        eventBus.post(new ModelTouchEvent(lastModel(), 1, 0));
        assertNull(gameOverEvent);
        assertEquals("3,3,1__000000", lastModel().toString());

        eventBus.post(new ModelTouchEvent(lastModel(), 0, 1));
        eventBus.post(new ModelTouchEvent(lastModel(), 0, 1));
        assertNotNull(gameOverEvent);
        assertEquals("3,3,______1__", lastModel().toString());
    }
}
