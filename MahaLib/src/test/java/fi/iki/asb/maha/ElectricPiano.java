package fi.iki.asb.maha;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import fi.iki.asb.maha.concurrent.OneThreadExecutor;
import fi.iki.asb.maha.event.GameOverEvent;
import fi.iki.asb.maha.event.ModelTouchEvent;
import fi.iki.asb.maha.event.ModelUpdatedEvent;
import fi.iki.asb.maha.event.StartNewGameEvent;
import fi.iki.asb.maha.model.Color;
import fi.iki.asb.maha.model.Dimension;
import fi.iki.asb.maha.model.Location;
import fi.iki.asb.maha.model.Model;

public class ElectricPiano extends MahaTestCase {

    /**
     * Size of board.
     */
    private static final Dimension DIMENSIONS = new Dimension(9, 11);

    /**
     * How many games should be played.
     */
    private static final int ROUNDS = 100;

    /**
     * After the auto player has been started into a background thread, the
     * thread that runs the tests tries to take an object from this queue.
     * The operation will block, as the queue is empty, until the auto player
     * finishes and puts a token object to the queue.
     */
    private final BlockingQueue<Object> queue =
            new LinkedBlockingQueue<Object>();

    /**
     * The player object that listens to events and creates the model
     * ModelTouchEvents. It scans the model left to right, top down and
     * performs a remove on the first possible location.
     */
    public Object player = new Object() {

        /**
         * How many games should be played.
         */
        private int count = ROUNDS;

        /**
         * The model has changed. Start looking for a removable block from
         * the upper left corner again.
         */
        @Subscribe
        public synchronized void onModelUpdated(ModelUpdatedEvent event) {
            Model model = event.getModel();
            Dimension dimensions = model.getDimensions();

            // System.err.println(model.toString());

            for (Location l1: dimensions.forEach()) {
                final Color c1 = model.getColorAt(l1);
                if (c1 == null)
                    continue;

                final Location l2 = l1.right();
                if (dimensions.contains(l2) && c1.equals(model.getColorAt(l2))) {
                    eventBus.post(new ModelTouchEvent(model, l1));
                    eventBus.post(new ModelTouchEvent(model, l1));
                    return;
                }
                
                final Location l3 = l1.down();
                if (dimensions.contains(l3) && c1.equals(model.getColorAt(l3))) {
                    eventBus.post(new ModelTouchEvent(model, l1));
                    eventBus.post(new ModelTouchEvent(model, l1));
                    return;
                }
            }
        }

        @Subscribe
        public synchronized void onGameOver(GameOverEvent event) {
            System.err.println(count + "----> " + scoreUpdatedEvents.getLast().getScore().getPoints());

            if (--count > 0) {
                eventBus.post(new StartNewGameEvent(DIMENSIONS));
            } else {
                try {
                    queue.put(Integer.valueOf(1));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    /**
     * The auto player is run in a separate thread.
     */
    @Override
    protected EventBus createEventBus() {
        return new AsyncEventBus(new OneThreadExecutor());
    }

    public void _testPlay() throws InterruptedException {
        eventBus.register(player);
        init(DIMENSIONS.getWidth(), DIMENSIONS.getHeight());

        // Wait for the auto player to put the token into the queue.
        queue.take();
    }
    
    public void testNil() {
    }
}