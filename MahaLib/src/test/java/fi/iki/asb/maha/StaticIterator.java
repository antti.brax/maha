package fi.iki.asb.maha;

import java.util.Iterator;

import fi.iki.asb.maha.model.Color;

public class StaticIterator implements Iterator<Color> {

    private final Color color;
    
    public StaticIterator(Color color) {
        this.color = color;
    }

    @Override
    public boolean hasNext() {
        return true;
    }
    
    @Override
    public Color next() {
        return color;
    }

}
