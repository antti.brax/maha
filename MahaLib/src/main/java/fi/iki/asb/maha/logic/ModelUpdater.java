package fi.iki.asb.maha.logic;

import java.util.LinkedList;
import java.util.List;

import fi.iki.asb.maha.event.ModelUpdatedEvent;
import fi.iki.asb.maha.model.Color;
import fi.iki.asb.maha.model.Dimension;
import fi.iki.asb.maha.model.Location;
import fi.iki.asb.maha.model.Model;
import fi.iki.asb.maha.model.UpdatedLocation;

/**
 * This class keeps track of changes made to a Model and generates
 * ModelUpdateEvent objects from the data.
 */
public class ModelUpdater {

    private final Model model;
    private final Dimension dimensions;
    private final Color[] colors;
    private List<Runnable> commands = new LinkedList<Runnable>();
    private List<UpdatedLocation> locations = new LinkedList<UpdatedLocation>();

    public ModelUpdater(final Model model) {
        this.model = model;
        this.dimensions = model.getDimensions();
        this.colors = new Color[dimensions.size()];
    }

    public final ModelUpdater set(
            final Location location,
            final Color color) {
        return set(location.column, location.row, color);
    }

    public ModelUpdater set(
            final int column,
            final int row,
            final Color color) {

        // XXX: The "restore persistent data" logic requires this method to
        // accept null color values. That should be implemented differently
        // and a null check should be added.

        commands.add(new Runnable() {
            @Override
            public void run() {
                colors[dimensions.index(column, row)] = color;
                locations.add(new UpdatedLocation(
                        null,
                        Location.valueOf(column, row)));
            }
        });

        return this;
    }

    public final ModelUpdater move(
            final Location from, final Location to) {
        return move(
                from.getColumn(), from.getRow(),
                to.getColumn(), to.getRow());
    }

    public ModelUpdater move(
            final int fromColumn, final int fromRow,
            final int toColumn, final int toRow) {
        commands.add(new Runnable() {
            @Override
            public void run() {
                colors[dimensions.index(toColumn, toRow)] = model.getColorAt(fromColumn, fromRow);
                colors[dimensions.index(fromColumn, fromRow)] = null;
                locations.add(new UpdatedLocation(
                        Location.valueOf(fromColumn, fromRow),
                        Location.valueOf(toColumn, toRow)));
            }
        });

        return this;
    }

    public ModelUpdater remove(
            final Location location) {
        commands.add(new Runnable() {
            @Override
            public void run() {
                colors[dimensions.index(location)] = null;
                locations.add(new UpdatedLocation(
                        location,
                        null));
            }
        });

        return this;
    }

    public final ModelUpdater remove(
            final int column, final int row) {
        return remove(Location.valueOf(column, row));
    }

    /**
     * Commit changes into a new Model and create a ModelUpdatedEvent from
     * them.
     *
     * The ModelUpdater object can not be used after this method has been
     * called. 
     */
    public ModelUpdatedEvent commit() {
        for (Location location: dimensions.forEach())
            colors[dimensions.index(location)] = model.getColorAt(location);

        for (Runnable r: commands)
            r.run();

        commands = null;

        return new ModelUpdatedEvent(
                new Model(model.getDimensions(), colors),
                locations);
    }
}