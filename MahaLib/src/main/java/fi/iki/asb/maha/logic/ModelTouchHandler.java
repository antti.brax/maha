package fi.iki.asb.maha.logic;

import java.util.HashSet;
import java.util.Set;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import fi.iki.asb.maha.event.ColorSelectionUpdatedEvent;
import fi.iki.asb.maha.event.ModelTouchEvent;
import fi.iki.asb.maha.model.Color;
import fi.iki.asb.maha.model.ColorSelection;
import fi.iki.asb.maha.model.Dimension;
import fi.iki.asb.maha.model.Location;
import fi.iki.asb.maha.model.Model;

/**
 * Component that listens to ModelTouchEvent objects and either selects
 * colors at the touch location or removes the previously selected colors.
 */
public class ModelTouchHandler {

    private final EventBus eventBus;

    private final Set<Location> selected;

    // =============================================================== //

    public ModelTouchHandler(final EventBus eventBus) {
        this.eventBus = eventBus;
        this.eventBus.register(this);
        this.selected = new HashSet<Location>();
    }

    // =============================================================== //

    @Subscribe
    public synchronized void onModelTouchEvent(final ModelTouchEvent event) {
        Model model = event.getModel();

        if (! model.contains(event.getLocation()))
            return;

        if (selected.contains(event.getLocation())) {
            // Remove the selected blocks.
            removeSelectedBlocks(model);
        } else {
            // Mark selected blocks.
            findConnectedColors(model, event.getLocation());
        }
    }

    // =============================================================== //

    private void removeSelectedBlocks(Model model) {
        final ModelUpdater updater = new ModelUpdater(model);

        // Build a table that has a Location object in each place the
        // model has a Color.
        final Dimension dim = model.getDimensions();
        final Location[] locs = new Location[dim.size()];
        for (Location origLoc: dim.forEach())
            if (model.getColorAt(origLoc) != null && selected.contains(origLoc) == false)
                locs[dim.index(origLoc)] = origLoc;

        // Collapse columns down.
        for (int column = 0; column < dim.getWidth(); column++)
            collapseColumn(model, locs, column);

        // Repack columns to the left.
        repackColumns(model, locs);

        // Clear the selected locations.
        for (Location l: selected)
            updater.remove(l);
        selected.clear();

        // For each location in the location table that is not in the original
        // location, move the color from the location in the index to the
        // location represented by the index.
        for (int row = dim.getHeight() - 1; row >= 0; row--) {
            for (int col = 0; col < dim.getWidth(); col++) {
                Location newLoc = Location.valueOf(col, row);
                final Location origLoc = locs[dim.index(newLoc)];
                if (origLoc != null && origLoc.equals(newLoc) == false) {
                    updater.move(origLoc, newLoc);
                }
            }
        }

        eventBus.post(updater.commit());
    }

    private void collapseColumn(
            final Model model,
            final Location[] locs,
            final int column) {
        final Dimension dim = model.getDimensions();
        int fromRow = model.getHeight() - 2;
        int toRow = model.getHeight() - 1;
        while (fromRow >= 0) {
            if (locs[dim.index(column, toRow)] != null) {
                toRow--;
                if (fromRow >= toRow)
                    fromRow = toRow - 1;
            } else if (locs[dim.index(column, fromRow)] == null) {
                fromRow--;
            } else {
                locs[dim.index(column, toRow)] = locs[dim.index(column, fromRow)];
                locs[dim.index(column, fromRow)] = null;

                toRow--;
                fromRow--;
            }
        }
    }

    private void repackColumns(
            final Model model,
            final Location[] locs) {
        final Dimension dim = model.getDimensions();
        final int bottomRow = dim.getHeight() - 1;
        int fromCol = 1;
        int toCol = 0;
        while (fromCol < dim.getWidth()) {
            if (locs[dim.index(toCol, bottomRow)] != null) {
                toCol++;
                if (fromCol <= toCol)
                    fromCol = toCol + 1;
            } else if (locs[dim.index(fromCol, bottomRow)] == null) {
                fromCol++;
            } else {
                for (int row = 0; row < dim.getHeight(); row++) {
                    locs[dim.index(toCol, row)] = locs[dim.index(fromCol, row)];
                    locs[dim.index(fromCol, row)] = null;
                }

                toCol++;
                fromCol++;
            }
        }
    }

    /**
     * Find all connected similar colors starting from the given location.
     */
    private void findConnectedColors(
            final Model model,
            final Location location) {
        selected.clear();

        final Color color = model.getColorAt(location);
        if (color != null) {
            doFindConnectedColors(model, location, color);
        }

        if (selected.size() < 2)
            selected.clear();

        eventBus.post(new ColorSelectionUpdatedEvent(
                new ColorSelection(new HashSet<Location>(selected))));
    }

    private void doFindConnectedColors(
            final Model model,
            final Location location,
            final Color expectedColor) {

        if (! model.contains(location))
            return;

        if (selected.contains(location))
            return;

        final Color colorHere = model.getColorAt(location);
        if (! expectedColor.equals(colorHere))
            return;

        selected.add(location);

        doFindConnectedColors(model, location.up(), expectedColor);
        doFindConnectedColors(model, location.down(), expectedColor);
        doFindConnectedColors(model, location.left(), expectedColor);
        doFindConnectedColors(model, location.right(), expectedColor);
    }
}
