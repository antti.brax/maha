package fi.iki.asb.maha.util;

import java.util.Iterator;
import java.util.Random;

import fi.iki.asb.maha.model.Color;

public class RandomIterator implements Iterator<Color> {

    private final Random rand = new Random();
    
    private final int numColors;
    
    public RandomIterator(int numColors) {
        this.numColors = numColors;
    }

    @Override
    public boolean hasNext() {
        return true;
    }
    
    @Override
    public Color next() {
        return Color.valueOf(rand.nextInt(numColors));
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }
}
