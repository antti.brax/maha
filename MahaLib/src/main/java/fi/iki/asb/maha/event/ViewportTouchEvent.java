package fi.iki.asb.maha.event;

import fi.iki.asb.maha.model.Location;

/**
 * Event denoting that a touch event targeting a location in the UI has been
 * performed. Location is in the coordinate space of the UI (pixels).
 */
public class ViewportTouchEvent {

    private final Location location;

    // =================================================================== //

    public ViewportTouchEvent(int column, int row) {
        this(Location.valueOf(column, row));
    }

    public ViewportTouchEvent(Location location) {
        this.location = location;
    }

    // =================================================================== //
    
    public Location getLocation() {
        return location;
    }

    // =================================================================== //

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((location == null) ? 0 : location.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ViewportTouchEvent other = (ViewportTouchEvent) obj;
        if (location == null) {
            if (other.location != null)
                return false;
        } else if (!location.equals(other.location))
            return false;
        return true;
    }
}
