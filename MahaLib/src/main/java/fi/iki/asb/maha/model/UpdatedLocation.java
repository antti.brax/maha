package fi.iki.asb.maha.model;

/**
 * An updated location containing information about the location from where
 * information was moved and to where it was moved.
 * <p>
 * If old location is null, then information was created.<br/>
 * If new location is null, then information was removed.<br/>
 * If neither is null, then information was moved.<br/>
 * If both are null, then an error occurred somewhere.<br/>
 */
public class UpdatedLocation {

    private final Location oldLocation;
    private final Location newLocation;

    // =================================================================== //

    public UpdatedLocation(Location oldLocation, Location newLocation) {
        super();
        this.oldLocation = oldLocation;
        this.newLocation = newLocation;
    }

    // =================================================================== //

    public final Location getNewLocation() {
        return newLocation;
    }

    public final Location getOldLocation() {
        return oldLocation;
    }

    public final boolean isAdd() {
        return newLocation != null && oldLocation == null;
    }

    public final boolean isRemove() {
        return newLocation == null && oldLocation != null;
    }

    public final boolean isMove() {
        return newLocation != null && oldLocation != null;
    }

    // =================================================================== //

    @Override
    public String toString() {
        return "UpdatedLocation [oldLocation=" + oldLocation + ", newLocation="
                + newLocation + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((newLocation == null) ? 0 : newLocation.hashCode());
        result = prime * result
                + ((oldLocation == null) ? 0 : oldLocation.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof UpdatedLocation)) {
            return false;
        }
        UpdatedLocation other = (UpdatedLocation) obj;
        if (newLocation == null) {
            if (other.newLocation != null) {
                return false;
            }
        } else if (!newLocation.equals(other.newLocation)) {
            return false;
        }
        if (oldLocation == null) {
            if (other.oldLocation != null) {
                return false;
            }
        } else if (!oldLocation.equals(other.oldLocation)) {
            return false;
        }
        return true;
    }
}
