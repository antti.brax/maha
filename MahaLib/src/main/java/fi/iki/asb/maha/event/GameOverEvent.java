package fi.iki.asb.maha.event;

import fi.iki.asb.maha.model.Model;

/**
 * Event denoting that a new game should be started.
 */
public class GameOverEvent {

    private final Model model;

    public GameOverEvent(Model model) {
        this.model = model;
    }

    public Model getModel() {
        return model;
    }
}
