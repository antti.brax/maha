package fi.iki.asb.maha.logic;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import fi.iki.asb.maha.event.GameOverEvent;
import fi.iki.asb.maha.event.ModelUpdatedEvent;
import fi.iki.asb.maha.model.Color;
import fi.iki.asb.maha.model.Location;
import fi.iki.asb.maha.model.Model;

/**
 * Class that receives ModelUpdatedEvent objects and detects when
 * there are no more moves that can be performed.
 */
public class GameOverDetector {

    private final EventBus eventBus;

    // =================================================================== //

    public GameOverDetector(final EventBus eventBus) {
        this.eventBus = eventBus;
        this.eventBus.register(this);
    }

    @Subscribe
    public synchronized void onModelUpdated(ModelUpdatedEvent event) {
        final Model model = event.getModel();
        boolean gameOver = true;
        // Board empty. Wait for refill.
        if (model.getColorAt(0, model.getHeight() - 1) == null)
            return;
        
        for (Location l1: model.getDimensions().forEach()) {
            final Color c = model.getColorAt(l1);
            if (c == null)
                continue;

            final Location l2 = l1.right();
            if (model.contains(l2) && c.equals(model.getColorAt(l2))) {
                gameOver = false;
                break;
            }

            final Location l3 = l1.down();
            if (model.contains(l3) && c.equals(model.getColorAt(l3))) {
                gameOver = false;
                break;
            }
        }

        if (gameOver)
            eventBus.post(new GameOverEvent(model));
    }

}
