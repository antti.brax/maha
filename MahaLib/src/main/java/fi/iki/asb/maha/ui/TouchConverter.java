package fi.iki.asb.maha.ui;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import fi.iki.asb.maha.event.ModelTouchEvent;
import fi.iki.asb.maha.event.ModelUpdatedEvent;
import fi.iki.asb.maha.event.ViewportChangedEvent;
import fi.iki.asb.maha.event.ViewportTouchEvent;
import fi.iki.asb.maha.model.Dimension;
import fi.iki.asb.maha.model.Location;
import fi.iki.asb.maha.model.Model;

public class TouchConverter {
    
    private final EventBus eventBus;

    private Dimension viewportDimensions = null;

    /**
     * XXX: This violates the statelessness principle.
     */
    private Model model = null;

    // =================================================================== //

    public TouchConverter(EventBus eventBus) {
        this.eventBus = eventBus;
        this.eventBus.register(this);
    }

    // =================================================================== //

    @Subscribe
    public synchronized void onViewportChanged(ViewportChangedEvent event) {
        viewportDimensions = event.getDimensions();
    }

    @Subscribe
    public synchronized void onModelUpdated(ModelUpdatedEvent event) {
        model = event.getModel();
    }

    @Subscribe
    public synchronized void onViewportTouch(ViewportTouchEvent event) {
        if (model == null || viewportDimensions == null)
            return;

        final Location touchLoc = event.getLocation();
        final double blockSize = viewportDimensions.getWidth()
                / (double) model.getWidth();

        final double col = touchLoc.column / blockSize;
        final double row = touchLoc.row / blockSize;

        eventBus.post(new ModelTouchEvent(model, (int) col, (int) row));
    }

}
