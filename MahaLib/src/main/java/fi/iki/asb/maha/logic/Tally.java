package fi.iki.asb.maha.logic;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import fi.iki.asb.maha.event.PointsScoredEvent;
import fi.iki.asb.maha.event.ScoredUpdatedEvent;
import fi.iki.asb.maha.event.StartNewGameEvent;
import fi.iki.asb.maha.model.Score;

/**
 * Calculator receives PointsScoredEvent objects
 * and keeps a tally on the mScore.
 */
public class Tally {

    private final EventBus mEventBus;
    
    private Score mScore = new Score();

    // =================================================================== //

    public Tally(final EventBus eventBus) {
        mEventBus = eventBus;
        mEventBus.register(this);
    }

    // =================================================================== //

    @Subscribe
    public synchronized void onStartNewGame(StartNewGameEvent event) {
        mScore = new Score();
        mEventBus.post(new ScoredUpdatedEvent(mScore));
    }

    @Subscribe
    public synchronized void onPointsScored(PointsScoredEvent event) {
        mScore = mScore.addPoints(event.getPoints());
        mEventBus.post(new ScoredUpdatedEvent(mScore));
    }

    /**
     * Score updated.
     */
    @Subscribe
    public synchronized void onScoredUpdated(ScoredUpdatedEvent event) {
        mScore = event.getScore();
    }

}
