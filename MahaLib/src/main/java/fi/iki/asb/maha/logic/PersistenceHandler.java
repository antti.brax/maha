package fi.iki.asb.maha.logic;

import com.google.common.eventbus.EventBus;
import fi.iki.asb.maha.model.Model;
import fi.iki.asb.maha.model.Score;

public class PersistenceHandler {

    /**
     * Key for storing model in persistent data. 
     */
    public static final String KEY_MODEL = "MODEL";

    /**
     * Key for storing points in persistent data. 
     */
    public static final String KEY_POINTS = "SCORE";

    // =================================================================== //

    private final EventBus eventBus;

    private Model model = null;

    private Score score = null;

    // =================================================================== //

    public PersistenceHandler(final EventBus eventBus) {
        this.eventBus = eventBus;
        this.eventBus.register(this);
    }

    /*
    public synchronized void onCollectPersistentData(CollectPersistentDataEvent event) {
        event.put(KEY_MODEL, model.toString());
        event.put(KEY_POINTS, String.valueOf(score.getPoints()));
    }

    public synchronized void onRestorePersistentData(RestorePersistentDataEvent event) {
        final String pointsStr = event.get(KEY_POINTS);
        if (pointsStr == null) {
            eventBus.post(new StartNewGameEvent(new Dimension(9, 11)));
            return;
        }
        final int points = Integer.valueOf(pointsStr);

        final String str = event.get(KEY_MODEL);
        if (str == null) {
            eventBus.post(new StartNewGameEvent(new Dimension(9, 11)));
            return;
        }

        final String[] arr = str.split(",");
        if (arr.length != 3) {
            eventBus.post(new StartNewGameEvent(new Dimension(9, 11)));
            return;
        }

        final int width = Integer.valueOf(arr[0]);
        final int height = Integer.valueOf(arr[1]);
        final Model model = new Model(new Dimension(width, height));
        final Dimension dimensions = model.getDimensions();
        final ModelUpdater updater = new ModelUpdater(model);
        for (int i = 0; i < arr[2].length(); i++) {
            final String s = arr[2].substring(i, i + 1);
            Color c = null;
            if (! s.equals("_"))
                c = Color.valueOf(Integer.valueOf(s));
            updater.set(dimensions.location(i), c);
        }

        System.err.println("SDFSDG" + points);

        eventBus.post(updater.commit());
        eventBus.post(new ScoredUpdatedEvent(new Score(points)));
    }
    */
}
