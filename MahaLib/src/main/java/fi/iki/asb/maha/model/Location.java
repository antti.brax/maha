package fi.iki.asb.maha.model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class Location {

    private static final Dimension CACHE_SIZE = new Dimension(9, 11);
    private static final Location[] CACHE = new Location[CACHE_SIZE.size()];

    static {
        for (int row = 0; row < CACHE_SIZE.getHeight(); row++)
            for (int col = 0; col < CACHE_SIZE.getWidth(); col++)
                CACHE[row * CACHE_SIZE.getWidth() + col] = new Location(col, row);
    }

    public static Location valueOf(int column, int row) {
        if (CACHE_SIZE.contains(column, row))
            return CACHE[row * CACHE_SIZE.getWidth() + column];
        else
            return new Location(column, row);
    }

    // =================================================================== //

    public final int column;
    public final int row;

    private Location(int column, int row) {
        super();
        this.column = column;
        this.row = row;
    }

    // =================================================================== //

    public Location up() {
        return new Location(column, row - 1);
    }

    public Location down() {
        return new Location(column, row + 1);
    }

    public Location left() {
        return new Location(column - 1, row);
    }

    public Location right() {
        return new Location(column + 1, row);
    }

    public int getColumn() {
        return column;
    }

    public int getRow() {
        return row;
    }

    public float distanceTo(Location that) {
        int c = this.column - that.column;
        int r = this.row - that.row;
        return (float) Math.sqrt(c * c + r * r);
    }

    /**
     * Return an iterator for locations from [0,0] to
     * [to.getColumn()-1,to.getRow()-1], left to right, top down.
     */
    public static Iterable<Location> forEach(final Location to) {
        return forEach(Location.valueOf(0, 0), to);
    }

    /**
     * Return an iterator for locations from [from.getColumn(),from.getRow()]
     * to [to.getColumn()-1,to.getRow()-1], left to right, top down.
     */
    public static Iterable<Location> forEach(final Location from, final Location to) {
        if (from.column >= to.column || from.row >= to.row)
            throw new IllegalArgumentException(from + " >= " + to);

        return new Iterable<Location>() {
            @Override
            public Iterator<Location> iterator() {
                return new LocationIterator(from, to);
            }
        };
    }

    // =================================================================== //

    @Override
    public String toString() {
        return "Location [column=" + column + ", row=" + row + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + column;
        result = prime * result + row;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Location other = (Location) obj;
        if (column != other.column)
            return false;
        if (row != other.row)
            return false;
        return true;
    }

    // =================================================================== //

    private static class LocationIterator implements Iterator<Location> {
        private final Location from;
        private final Location to;
        private int col;
        private int row;

        private LocationIterator(Location from, Location to) {
            this.from = from;
            this.to = to;

            this.col = from.column;
            this.row = from.row;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean hasNext() {
            return col < to.getColumn() && row < to.getRow();
        }

        @Override
        public Location next() {
            if (hasNext()) {
                final Location next = Location.valueOf(col, row);
                col = col + 1;
                if (col >= to.column) {
                    col = from.column;
                    row++;
                }
                return next;
            } else {
                throw new NoSuchElementException("Iterator exhausted");
            }
        }
    }

    // =================================================================== //

    private static final byte VERSION_1 = 1;

    public static void write(Location location, ObjectOutputStream out) throws IOException {
        out.writeByte(VERSION_1);
        out.writeInt(location.getColumn());
        out.writeInt(location.getRow());
    }

    public static Location read(ObjectInputStream in) throws IOException {
        byte version = in.readByte();

        if (version <= VERSION_1) {
            return Location.valueOf(in.readInt(), in.readInt());
        }

        throw new IOException("Unknown version " + version);
    }
}
