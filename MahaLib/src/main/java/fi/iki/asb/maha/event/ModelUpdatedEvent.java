package fi.iki.asb.maha.event;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import fi.iki.asb.maha.model.Model;
import fi.iki.asb.maha.model.UpdatedLocation;

public class ModelUpdatedEvent {

    private final Model model;

    private final Collection<UpdatedLocation> updates;

    public ModelUpdatedEvent(
            Model model,
            Collection<UpdatedLocation> updates) {
        this.model = model;
        this.updates = updates;
    }

    public Model getModel() {
        return model;
    }
    
    public Collection<UpdatedLocation> getLocations() {
        return Collections.unmodifiableCollection(updates);
    }

    @Override
    public String toString() {
        return "ModelUpdatedEvent [updates="
                + (updates != null ? toString(updates) : null) + "]";
    }

    private String toString(Collection<?> collection) {
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        int i = 0;
        for (Iterator<?> iterator = collection.iterator(); iterator.hasNext(); i++) {
            if (i > 0)
                builder.append(", ");
            builder.append(iterator.next());
        }
        builder.append("]");
        return builder.toString();
    }

}
