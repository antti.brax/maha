package fi.iki.asb.maha.logic;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import fi.iki.asb.maha.event.GameOverEvent;
import fi.iki.asb.maha.event.ModelClearedEvent;
import fi.iki.asb.maha.event.ModelUpdatedEvent;
import fi.iki.asb.maha.event.PointsScoredEvent;
import fi.iki.asb.maha.event.ScoredUpdatedEvent;
import fi.iki.asb.maha.event.StartNewGameEvent;
import fi.iki.asb.maha.event.StatisticsUpdatedEvent;
import fi.iki.asb.maha.event.ViewportTouchEvent;
import fi.iki.asb.maha.model.Score;
import fi.iki.asb.maha.model.Statistics;
import fi.iki.asb.maha.model.UpdatedLocation;

/**
 * Calculator receives ModelUpdatedEvent and ModelClearedEvent objects
 * and calculates how much points the player scored.
 */
public class Statistician {

    private final EventBus mEventBus;

    private Statistics mStatistics = new Statistics();

    private Score mCurrentScore = new Score();

    // =================================================================== //

    public Statistician(final EventBus eventBus) {
        mEventBus = eventBus;
        mEventBus.register(this);
    }

    // =================================================================== //

    @Subscribe
    public synchronized void onGameOver(GameOverEvent event) {
        Long highScore = mStatistics.getHighScore();
        Long lowScore = mStatistics.getLowScore();

        Statistics statistics = mStatistics;
        if (highScore == -1 || mCurrentScore.getPoints() > highScore)
            statistics = statistics.setHighScore(mCurrentScore.getPoints());
        if (lowScore == -1 || mCurrentScore.getPoints() < lowScore)
            statistics = statistics.setLowScore(mCurrentScore.getPoints());

        if (statistics != mStatistics) {
            mStatistics = statistics;
            mEventBus.post(new StatisticsUpdatedEvent(statistics));
        }
    }

    @Subscribe
    public synchronized void onPointsScored(PointsScoredEvent event) {
        mStatistics = mStatistics
                .addPointsScored(event.getPoints())
                .addBlocksRemoved(event.getBlocksRemoved());
        mEventBus.post(new StatisticsUpdatedEvent(mStatistics));
    }

    @Subscribe
    public synchronized void onViewportTouch(ViewportTouchEvent event) {
        mStatistics = mStatistics
	    .addViewportTouched();
        mEventBus.post(new StatisticsUpdatedEvent(mStatistics));
    }

    @Subscribe
    public synchronized void onScoreUpdated(ScoredUpdatedEvent event) {
        mCurrentScore = event.getScore();
    }

    @Subscribe
    public synchronized void onStartNewGame(StartNewGameEvent event) {
        mStatistics = mStatistics
                .addGamesStarted();
        mEventBus.post(new StatisticsUpdatedEvent(mStatistics));
    }

    @Subscribe
    public synchronized  void onStatisticsUpdated(StatisticsUpdatedEvent event) {
        mStatistics = event.getStatistics();
    }
}
