package fi.iki.asb.maha.model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Color {

    private static final Color[] CACHE = new Color[10];

    static {
        for (int i = 0; i < CACHE.length; i++)
            CACHE[i] = new Color(i);
    }

    public static Color valueOf(int color) {
        if (color >= 0 && color < CACHE.length)
            return CACHE[color];
        else
            return new Color(color);
    }

    // =================================================================== //

    private final int value;

    private Color(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Color [value=" + value + "]";
    }

    // =================================================================== //

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + value;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Color other = (Color) obj;
        if (value != other.value)
            return false;
        return true;
    }

    // =================================================================== //

    private static final byte VERSION_1 = 1;

    public static void write(Color color, ObjectOutputStream out) throws IOException {
        out.writeByte(VERSION_1);
        if (color == null)
            out.writeObject(null);
        else
            out.writeObject(Integer.valueOf(color.getValue()));
    }

    public static Color read(ObjectInputStream in) throws IOException {
        byte version = in.readByte();

        if (version <= VERSION_1) {
            try {
                Integer color = (Integer) in.readObject();
                if (color == null)
                    return null;
                else
                    return Color.valueOf(color);
            } catch (ClassNotFoundException e) {
                throw new IOException(e);
            }
        }

        throw new IOException("Unknown version " + version);
    }
}
