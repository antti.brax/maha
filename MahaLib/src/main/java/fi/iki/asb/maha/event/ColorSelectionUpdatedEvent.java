package fi.iki.asb.maha.event;

import fi.iki.asb.maha.model.ColorSelection;

public class ColorSelectionUpdatedEvent {

    private final ColorSelection selection;

    public ColorSelectionUpdatedEvent(final ColorSelection selection) {
        this.selection = selection;
    }

    public ColorSelection getSelection() {
        return selection;
    }
}
