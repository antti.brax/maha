package fi.iki.asb.maha.event;

public class PointsScoredEvent {

    private final int points;

    private final int blocksRemoved;

    public PointsScoredEvent(int points, int blocksRemoved) {
        this.points = points;
        this.blocksRemoved = blocksRemoved;
    }

    public int getPoints() {
        return points;
    }

    public int getBlocksRemoved() {
        return blocksRemoved;
    }

    @Override
    public String toString() {
        return "PointsScoredEvent{" +
                "points=" + points +
                ", blocksRemoved=" + blocksRemoved +
                '}';
    }
}
