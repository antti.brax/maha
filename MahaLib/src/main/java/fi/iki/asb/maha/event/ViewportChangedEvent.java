package fi.iki.asb.maha.event;

import fi.iki.asb.maha.model.Dimension;

/**
 * Event that notifies about changes in the UI viewport size.
 */
public class ViewportChangedEvent {

    private final Dimension dimensions;

    public ViewportChangedEvent(Dimension dimensions) {
        this.dimensions = dimensions;
    }

    public Dimension getDimensions() {
        return dimensions;
    }

}
