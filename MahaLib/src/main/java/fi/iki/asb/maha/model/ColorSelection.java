package fi.iki.asb.maha.model;

import java.util.Collection;
import java.util.Collections;

/**
 * Container class for the colors that are currently selected.
 */
public class ColorSelection {

    private final Collection<Location> locations;

    public ColorSelection(Collection<Location> locations) {
        this.locations = locations;
    }

    public Collection<Location> getLocations() {
        return Collections.unmodifiableCollection(locations);
    }
}
