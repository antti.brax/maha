package fi.iki.asb.maha.event;

import fi.iki.asb.maha.model.Score;

public class ScoredUpdatedEvent {

    private final Score score;

    public ScoredUpdatedEvent(Score score) {
        this.score = score;
    }

    public Score getScore() {
        return score;
    }

    @Override
    public String toString() {
        return "ScoredUpdatedEvent [score=" + score + "]";
    }

}
