package fi.iki.asb.maha.logic;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import fi.iki.asb.maha.Constants;
import fi.iki.asb.maha.event.GameOverEvent;
import fi.iki.asb.maha.event.StartNewGameEvent;
import fi.iki.asb.maha.event.ViewportTouchEvent;

public class NewGameStarter {

    private final EventBus mEventBus;

    private boolean mGameOver = false;

    public NewGameStarter(EventBus eventBus) {
        mEventBus = eventBus;
        mEventBus.register(this);
    }

    @Subscribe
    public synchronized void onGameOver(GameOverEvent event) {
        mGameOver = true;
    }

    @Subscribe
    public synchronized void onViewportTouch(ViewportTouchEvent event) {
        if (! mGameOver)
            return;

        mGameOver = false;
        mEventBus.post(new StartNewGameEvent(Constants.SIZE));
    }
}
