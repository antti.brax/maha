package fi.iki.asb.maha.event;

import fi.iki.asb.maha.model.Location;
import fi.iki.asb.maha.model.Model;

/**
 * Event denoting that a touch event targeting a location in the model has
 * been performed. Location is in the coordinate space of the model
 * (columns,rows).
 */
public class ModelTouchEvent {

    private final Model model;

    private final Location location;

    // =================================================================== //

    public ModelTouchEvent(Model model, int column, int row) {
        this(model, Location.valueOf(column, row));
    }

    public ModelTouchEvent(Model model, Location location) {
        this.model = model;
        this.location = location;
    }

    // =================================================================== //
    
    public Model getModel() {
        return model;
    }

    public Location getLocation() {
        return location;
    }

    // =================================================================== //

    @Override
    public String toString() {
        return "ModelTouchEvent [location=" + location + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((location == null) ? 0 : location.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ModelTouchEvent other = (ModelTouchEvent) obj;
        if (location == null) {
            if (other.location != null)
                return false;
        } else if (!location.equals(other.location))
            return false;
        return true;
    }
}
