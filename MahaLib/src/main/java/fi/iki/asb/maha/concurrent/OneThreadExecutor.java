package fi.iki.asb.maha.concurrent;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * An executor that runs commands in one separate thread.
 */
public class OneThreadExecutor implements Executor {

    private final BlockingQueue<Runnable> queue;

    private final Thread executorThread;

    // =================================================================== //

    public OneThreadExecutor() {
        queue = new LinkedBlockingQueue<Runnable>();
        executorThread = new Thread() {
            @Override
            public void run() {
                while (true) {
                    try {
                        queue.take().run();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        };
        executorThread.setDaemon(true);
        executorThread.start();
    }

    // =================================================================== //

    @Override
    public void execute(Runnable command) {
        try {
            queue.put(command);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public void interrupt() {
        executorThread.interrupt();
    }

    public boolean isEmpty() {
        return queue.isEmpty();
    }
}
