package fi.iki.asb.maha.model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Current score.
 */
public class Score {

    private final long mPoints;

    // =================================================================== //

    public Score() {
        this(0);
    }

    private Score(long points) {
        mPoints = points;
    }

    // =================================================================== //

    public long getPoints() {
        return mPoints;
    }

    public Score addPoints(long points) {
        return new Score(mPoints + points);
    }

    @Override
    public String toString() {
        return "Score{" +
                "mPoints=" + mPoints +
                '}';
    }

    // =================================================================== //

    private static final byte VERSION_1 = 1;

    public static void write(Score score, ObjectOutputStream out) throws IOException {
        out.writeByte(VERSION_1);
        out.writeLong(score.getPoints());
    }

    public static Score read(ObjectInputStream in) throws IOException {
        byte version = in.readByte();

        if (version <= VERSION_1) {
            long points = in.readLong();
            return new Score(points);
        }

        throw new IOException("Unknown version " + version);
    }
}
