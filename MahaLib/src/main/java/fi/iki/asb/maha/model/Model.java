package fi.iki.asb.maha.model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * The model.
 */
public class Model {

    /**
     * Dimensions of the model.
     */
    private final Dimension dimension;

    /**
     * The actual data.
     */
    private final Color[] colors;

    // =================================================================== //

    public Model(Dimension dimension) {
        this.dimension = dimension;
        this.colors = new Color[dimension.size()];
    }

    public Model(Dimension dimension, Color[] colors) {
        this(dimension);
        System.arraycopy(colors, 0, this.colors, 0, colors.length);
    }

    // =================================================================== //

    public final int getWidth() {
        return dimension.getWidth();
    }

    public final int getHeight() {
        return dimension.getHeight();
    }

    public Dimension getDimensions() {
        return dimension;
    }

    public final boolean contains(Location location) {
        return contains(location.column, location.row);
    }

    public boolean contains(int column, int row) {
        return dimension.contains(column, row);
    }

    public final Color getColorAt(Location l) {
        return getColorAt(l.column, l.row);
    }

    public Color getColorAt(int column, int row) {
        if (! contains(column, row))
            return null;
        else
            return colors[dimension.index(column, row)];
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(dimension.getWidth() * dimension.getHeight()); 
        sb.append(getWidth()).append(',').append(getHeight()).append(',');
        for (Location location: dimension.forEach()) {
            Color color = getColorAt(location);
            if (color == null)
                sb.append("_");
            else
                sb.append(color.getValue());
        }
        return sb.toString();
    }

    public String toPrettyString() {
        StringBuilder sb = new StringBuilder(dimension.getWidth() * dimension.getHeight()); 
        
        sb.append('+');
        for (int col = 0; col < getWidth(); col++)
            sb.append('-');
        sb.append("+\n");
        for (int row = 0; row < getHeight(); row++) {
            sb.append("|");
            for (int col = 0; col < getWidth(); col++) {
                Color color = getColorAt(col, row);
                if (color == null)
                    sb.append("_");
                else
                    sb.append(color.getValue());
            }
            sb.append("|\n");
        }
        sb.append('+');
        for (int col = 0; col < getWidth(); col++)
            sb.append('-');
        sb.append('+');
        return sb.toString();
    }

    // =================================================================== //

    private static final byte VERSION_1 = 1;

    public static void write(Model model, ObjectOutputStream out) throws IOException {
        out.writeByte(VERSION_1);
        Dimension.write(model.getDimensions(), out);
        for (Location l: model.getDimensions().forEach())
            Color.write(model.getColorAt(l), out);
    }

    public static Model read(ObjectInputStream in) throws IOException {
        byte version = in.readByte();

        if (version <= VERSION_1) {
            Dimension dimension = Dimension.read(in);
            Color[] colors = new Color[dimension.size()];
            for (int i = 0; i < colors.length; i++)
                colors[i] = Color.read(in);

            return new Model(dimension, colors);
        }

        throw new IOException("Unknown version " + version);
    }
}
