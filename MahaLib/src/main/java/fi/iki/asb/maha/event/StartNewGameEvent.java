package fi.iki.asb.maha.event;

import fi.iki.asb.maha.model.Dimension;

/**
 * Event denoting that a new game should be started.
 */
public class StartNewGameEvent {
    
    private final Dimension dimensions;

    public StartNewGameEvent(Dimension dimensions) {
        this.dimensions = dimensions;
    }

    public Dimension getDimensions() {
        return dimensions;
    }
}
