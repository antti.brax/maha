package fi.iki.asb.maha.model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Statistics implements Cloneable {

    private long mBlocksRemoved = 0;

    private long mGamesStarted = 0;

    private long mHighScore = -1;

    private long mLowScore = -1;

    private long mPointsScored = 0;

    private long mViewportTouched = 0;

    // =================================================================== //

    public Statistics() {
    }

    // =================================================================== //

    public long getBlocksRemoved() {
        return mBlocksRemoved;
    }

    public long getGamesStarted() {
        return mGamesStarted;
    }

    public long getHighScore() {
        return mHighScore;
    }

    public long getLowScore() {
        return mLowScore;
    }

    public long getPointsScored() {
        return mPointsScored;
    }

    public long getViewportTouched() {
        return mViewportTouched;
    }

    // =================================================================== //

    public Statistics addBlocksRemoved(long blocksRemoved) {
        Statistics copy = (Statistics) clone();
        copy.mBlocksRemoved += blocksRemoved;
        return copy;
    }

    public Statistics addPointsScored(long pointsScored) {
        Statistics copy = (Statistics) clone();
        copy.mPointsScored += pointsScored;
        return copy;
    }

    public Statistics addGamesStarted() {
        Statistics copy = (Statistics) clone();
        copy.mGamesStarted++;
        return copy;
    }

    public Statistics addViewportTouched() {
        Statistics copy = (Statistics) clone();
        copy.mViewportTouched++;
        return copy;
    }

    public Statistics setHighScore(long highScore) {
        Statistics copy = (Statistics) clone();
        copy.mHighScore = highScore;
        return copy;
    }

    public Statistics setLowScore(long lowScore) {
        Statistics copy = (Statistics) clone();
        copy.mLowScore = lowScore;
        return copy;
    }

    // =================================================================== //

    protected Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String toString() {
        return "Statistics{" +
                "mBlocksRemoved=" + mBlocksRemoved +
                ", mGamesStarted=" + mGamesStarted +
                ", mHighScore=" + mHighScore +
                ", mLowScore=" + mLowScore +
                ", mPointsScored=" + mPointsScored +
                ", mViewportTouched=" + mViewportTouched +
                '}';
    }

    // =================================================================== //

    private static final byte VERSION_1 = 1;

    public static void write(Statistics stats, ObjectOutputStream out) throws IOException {
        out.writeByte(VERSION_1);
        out.writeLong(stats.getBlocksRemoved());
        out.writeLong(stats.getGamesStarted());
        out.writeLong(stats.getHighScore());
        out.writeLong(stats.getLowScore());
        out.writeLong(stats.getPointsScored());
        out.writeLong(stats.getViewportTouched());
    }

    public static Statistics read(ObjectInputStream in) throws IOException {
        byte version = in.readByte();

        if (version <= VERSION_1) {
            final Statistics stats = new Statistics();
            stats.mBlocksRemoved = in.readLong();
            stats.mGamesStarted = in.readLong();
            stats.mHighScore = in.readLong();
            stats.mLowScore = in.readLong();
            stats.mPointsScored = in.readLong();
            stats.mViewportTouched = in.readLong();
            return stats;
        }

        throw new IOException("Unknown version " + version);
    }
}
