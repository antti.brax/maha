package fi.iki.asb.maha.logic;

import java.util.Iterator;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import fi.iki.asb.maha.event.ModelClearedEvent;
import fi.iki.asb.maha.event.ModelUpdatedEvent;
import fi.iki.asb.maha.event.StartNewGameEvent;
import fi.iki.asb.maha.model.Color;
import fi.iki.asb.maha.model.Location;
import fi.iki.asb.maha.model.Model;

/**
 * Component that listens to ModelUpdatedEvent objects and automatically
 * fills the model with colors when it becomes empty.
 */
public class ModelFiller {

    private final EventBus eventBus;
    
    private final Iterator<Color> iterator;

    // =================================================================== //

    public ModelFiller(final EventBus eventBus,
            final Iterator<Color> iterator) {
        this.eventBus = eventBus;
        this.iterator = iterator;
        this.eventBus.register(this);
    }

    // =================================================================== //

    @Subscribe
    public synchronized void onStartNewGame(StartNewGameEvent event) {
        final Model model = new Model(event.getDimensions());
        fill(model);
    }

    @Subscribe
    public void onModelUpdated(ModelUpdatedEvent event) {
        Model model = event.getModel();
        if (model.getColorAt(0, model.getHeight() - 1) == null) {
            eventBus.post(new ModelClearedEvent());
            fill(event.getModel());
        }
    }

    private void fill(Model model) {
        final ModelUpdater updater = new ModelUpdater(model);
        for (Location location: model.getDimensions().forEach())
            updater.set(location, iterator.next());
        eventBus.post(updater.commit());
    }
}
