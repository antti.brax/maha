package fi.iki.asb.maha.event;

import fi.iki.asb.maha.model.Score;
import fi.iki.asb.maha.model.Statistics;

public class StatisticsUpdatedEvent {

    private final Statistics mStatistics;

    public StatisticsUpdatedEvent(Statistics statistics) {
        mStatistics = statistics;
    }

    public Statistics getStatistics() {
        return mStatistics;
    }

    @Override
    public String toString() {
        return "StatisticsUpdatedEvent{" +
                "mStatistics=" + mStatistics +
                '}';
    }
}
