package fi.iki.asb.maha.logic;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import fi.iki.asb.maha.event.ModelClearedEvent;
import fi.iki.asb.maha.event.ModelUpdatedEvent;
import fi.iki.asb.maha.event.PointsScoredEvent;
import fi.iki.asb.maha.model.UpdatedLocation;

/**
 * Calculator receives ModelUpdatedEvent and ModelClearedEvent objects
 * and calculates how much points the player scored.
 */
public class PointsCalculator {

    private final EventBus eventBus;

    // =================================================================== //

    public PointsCalculator(final EventBus eventBus) {
        this.eventBus = eventBus;
        this.eventBus.register(this);
    }

    // =================================================================== //

    /**
     * Points scored: <code>250</code>.
     */
    @Subscribe
    public synchronized void onModelCleared(ModelClearedEvent event) {
        eventBus.post(new PointsScoredEvent(250, 0));
    }

    /**
     * Points scored: <code>(colors removed - 2) ^ 2</code>.
     */
    @Subscribe
    public synchronized void onModelUpdated(ModelUpdatedEvent event) {
        int removeCounter = 0;
        for (UpdatedLocation updatedLocation: event.getLocations())
            if (updatedLocation.getNewLocation() == null)
                removeCounter++;

        int points = 0;
        if (removeCounter > 2)
            points = (int) Math.pow(removeCounter - 2, 2);

        if (points != 0 || removeCounter != 0)
            eventBus.post(new PointsScoredEvent(points, removeCounter));
    }

}
