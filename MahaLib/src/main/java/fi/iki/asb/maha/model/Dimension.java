package fi.iki.asb.maha.model;

import com.google.common.base.Objects;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Dimension {

    private final int width;

    private final int height;

    // =================================================================== //

    public Dimension(int width, int height) {
        this.width = width;
        this.height = height;
    }

    // =================================================================== //

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
    
    public int size() {
        return width * height;
    }

    // =================================================================== //

    public final boolean contains(Location location) {
        return contains(location.column, location.row);
    }

    public boolean contains(int column, int row) {
        return column >= 0 && column < width && row >= 0 && row < height;
    }

    // =================================================================== //

    /**
     * Helper method for calculating <code>column + row * width</code>.
     */
    public final int index(Location location) {
        return index(location.column, location.row);
    }

    /**
     * Helper method for calculating <code>column + row * width</code>.
     */
    public int index(int column, int row) {
        if (contains(column, row))
            return column + row * width;
        else
            throw new ArrayIndexOutOfBoundsException("[" + column + ", " + row + "]");
    }

    /**
     * Helper method for calculating <code>index % getWidth(), index / getWidth()</code>.
     */
    public Location location(int index) {
        int row = index / width;
        int column = index % width;
        if (contains(column, row))
            return Location.valueOf(column, row);
        else
            throw new ArrayIndexOutOfBoundsException("[" + index + "]");
    }

    // =================================================================== //

    /**
     * Return an iterator for locations from [0,0] to [width-1, height-1],
     * left to right, top down.
     */
    public Iterable<Location> forEach() {
        return Location.forEach(
                Location.valueOf(0, 0),
                Location.valueOf(width, height));
    }

    // =================================================================== //

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Dimension dimension = (Dimension) o;

        if (width != dimension.width) return false;
        return height == dimension.height;

    }

    @Override
    public int hashCode() {
        int result = width;
        result = 31 * result + height;
        return result;
    }

    // =================================================================== //

    private static final byte VERSION_1 = 1;

    public static void write(Dimension dimension, ObjectOutputStream out) throws IOException {
        out.writeByte(VERSION_1);
        out.writeInt(dimension.getWidth());
        out.writeInt(dimension.getHeight());
    }

    public static Dimension read(ObjectInputStream in) throws IOException {
        byte version = in.readByte();

        if (version <= VERSION_1) {
            return new Dimension(
                    in.readInt(),
                    in.readInt());
        }

        throw new IOException("Unknown version " + version);
    }
}
