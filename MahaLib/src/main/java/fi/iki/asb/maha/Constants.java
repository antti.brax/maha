package fi.iki.asb.maha;

import fi.iki.asb.maha.model.Dimension;

public class Constants {

    public static final Dimension SIZE = new Dimension(9, 11);

}
