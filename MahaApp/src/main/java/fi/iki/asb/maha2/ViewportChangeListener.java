package fi.iki.asb.maha2;

import android.view.SurfaceHolder;

import com.google.common.eventbus.EventBus;

import fi.iki.asb.maha.event.ViewportChangedEvent;
import fi.iki.asb.maha.model.Dimension;
import fi.iki.asb.maha2.event.SurfaceChangedEvent;

public class ViewportChangeListener implements SurfaceHolder.Callback {

    private final EventBus mEventBus;

    public ViewportChangeListener(EventBus eventBus) {
        mEventBus = eventBus;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        mEventBus.post(new ViewportChangedEvent(new Dimension(width, height)));
        mEventBus.post(new SurfaceChangedEvent(holder));
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        mEventBus.post(new ViewportChangedEvent(null));
        mEventBus.post(new SurfaceChangedEvent(null));
    }
}
