package fi.iki.asb.maha2.ui;

/**
 * Commands registered to the visual model are executed every time a repaint is triggered.
 */
public interface Command {

    /**
     * Execute the command.
     *
     * @param currentTime Current time in milliseconds since the game started.
     */
    void execute(long currentTime);

}
