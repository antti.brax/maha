package fi.iki.asb.maha2.ui.controller;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.Log;

import com.google.common.eventbus.Subscribe;

import fi.iki.asb.maha.Constants;
import fi.iki.asb.maha.event.ScoredUpdatedEvent;
import fi.iki.asb.maha.event.StartNewGameEvent;
import fi.iki.asb.maha.event.ViewportChangedEvent;
import fi.iki.asb.maha.model.Dimension;
import fi.iki.asb.maha2.R;
import fi.iki.asb.maha2.ui.Tools;
import fi.iki.asb.maha2.ui.VisualModelCallback;
import fi.iki.asb.maha2.ui.paintable.Text;

public class ScoreTextController {

    private static final String TAG = "ScoreTextController";

    // =================================================================== //

    private final VisualModelCallback mCallback;

    private final RectF mTextBounds;

    private final Paint mTextStyle;

    private final Text mScoreText;

    private final float mMargin;

    private Dimension mViewportSize;

    // =================================================================== //

    public ScoreTextController(
            final Context context,
            final VisualModelCallback callback) {
        mCallback = callback;

        mTextStyle = new Paint();
        mTextStyle.setARGB(0xFF, 0xFF, 0xFF, 0xFF);

        mScoreText = new Text("0", mTextStyle);
        mScoreText.setTextSize(context.getResources().getDimensionPixelSize(R.dimen.ScoreTextSize));
        mScoreText.setLayerHeight(1);

        mViewportSize = null;
        mTextBounds = new RectF();
        mMargin = Tools.dpToPx(context, 2.0f);

        mCallback.addPaintable(mScoreText);
    }

    private void relocate() {
        mScoreText.getBounds(mTextBounds);
        mScoreText.setX((mViewportSize.getWidth() - mTextBounds.width()) / 2.0f);
        mScoreText.setY(mViewportSize.getWidth() / (float) Constants.SIZE.getWidth() * 11.0f + mMargin);
    }

    @Subscribe
    public void onViewportChange(ViewportChangedEvent event) {

        Log.d(TAG, "onViewportChange()");

        mViewportSize = event.getDimensions();
        mCallback.removePaintable(mScoreText);

        if (mViewportSize != null) {
            relocate();
            mCallback.addPaintable(mScoreText);
        }
    }

    @Subscribe
    public void onStartNewGame(StartNewGameEvent event) {

        Log.d(TAG, "onStartNewGame()");

        mCallback.removePaintable(mScoreText);
        mCallback.addPaintable(mScoreText);
    }

    @Subscribe
    public void onScoredUpdated(ScoredUpdatedEvent event) {
        mScoreText.setText(String.valueOf(event.getScore().getPoints()));

        if (mViewportSize != null) {
            relocate();
            mCallback.flagChange();
        }
    }
}
