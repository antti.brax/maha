package fi.iki.asb.maha2.ui.controller;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.common.eventbus.Subscribe;

import fi.iki.asb.maha.event.GameOverEvent;
import fi.iki.asb.maha.event.ViewportChangedEvent;
import fi.iki.asb.maha.model.Dimension;
import fi.iki.asb.maha2.R;
import fi.iki.asb.maha2.ui.VisualModelCallback;
import fi.iki.asb.maha2.ui.paintable.GameOver;
import fi.iki.asb.maha2.ui.paintable.Text;

public class GameOverController {

    private final VisualModelCallback mCallback;

    private final GameOver mGameOver;

    private final RectF mGameOverTextBounds;

    private final Text mGameOverText;

    // =================================================================== //

    public GameOverController(
            final Context context,
            final VisualModelCallback callback) {
        mCallback = callback;

        mGameOver = new GameOver(
                ContextCompat.getColor(context, R.color.transparentBlack50));


        mGameOverTextBounds = new RectF();

        Paint gameOverTextStyle = new Paint();
        gameOverTextStyle.setARGB(0xFF, 0xFF, 0xFF, 0xFF);

        mGameOverText = new Text("GAME OVER", gameOverTextStyle);
        mGameOverText.setTextSize(context.getResources().getDimensionPixelSize(R.dimen.ScoreTextSize));
        mGameOverText.setLayerHeight(300);
        mGameOverText.getBounds(mGameOverTextBounds);
    }

    @Subscribe
    public void onViewportChange(ViewportChangedEvent event) {
        boolean putBack = mCallback.removePaintable(mGameOverText);

        Dimension viewportSize = event.getDimensions();
        if (viewportSize != null) {
            mGameOverText.setX((viewportSize.getWidth() - mGameOverTextBounds.right) / 2.0f);
            mGameOverText.setY((viewportSize.getHeight() - mGameOverTextBounds.bottom) / 2.0f);
            if (putBack)
                mCallback.addPaintable(mGameOverText);
        }
    }

    @Subscribe
    public void onGameOver(GameOverEvent event) {
        mCallback.removePaintable(mGameOver);
        mCallback.removePaintable(mGameOverText);
        mCallback.addPaintable(mGameOver);
        mCallback.addPaintable(mGameOverText);
    }
}
