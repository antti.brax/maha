package fi.iki.asb.maha2.ui;

import android.graphics.Canvas;

/**
 * Created by antti on 2/23/17.
 */

public interface Paintable {

    /**
     * Get a number that represents the height of the imaginary layer this paintable is on.
     * Used to sort paintables so that those on the upper layers overshadow the lower ones.
     */
    int getLayerHeight();

    /**
     * Draw this on the canvas.
     */
    void drawOn(Canvas canvas);

}
