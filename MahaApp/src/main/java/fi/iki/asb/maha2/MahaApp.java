package fi.iki.asb.maha2;

import android.app.Application;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.EventBus;

import fi.iki.asb.maha.concurrent.OneThreadExecutor;
import fi.iki.asb.maha.logic.GameOverDetector;
import fi.iki.asb.maha.logic.ModelFiller;
import fi.iki.asb.maha.logic.ModelTouchHandler;
import fi.iki.asb.maha.logic.NewGameStarter;
import fi.iki.asb.maha.logic.PointsCalculator;
import fi.iki.asb.maha.logic.Statistician;
import fi.iki.asb.maha.logic.Tally;
import fi.iki.asb.maha.ui.TouchConverter;
import fi.iki.asb.maha.util.RandomIterator;
import fi.iki.asb.maha2.event.RestoreGameStateEvent;
import fi.iki.asb.maha2.ui.Painter;
import fi.iki.asb.maha2.ui.VisualModel;

public class MahaApp extends Application {

    private static final String TAG = "MahaApp";

    private EventBus mEventBus;

    @Override
    public void onCreate() {
        super.onCreate();

        Log.d(TAG, "onCreate()");

        mEventBus = new AsyncEventBus(new OneThreadExecutor());
        mEventBus.register(new DeadEventListener());

        new Tally(mEventBus);
        new Statistician(mEventBus);
        new ModelFiller(mEventBus, new RandomIterator(4));
        new ModelTouchHandler(mEventBus);
        new TouchConverter(mEventBus);
        new PointsCalculator(mEventBus);
        new VisualModel(this, mEventBus);
        new Painter(mEventBus);
        new GameStateSaver(mEventBus, getSharedPreferences("GAME_STATE", 0));
        new GameOverDetector(mEventBus);
        new NewGameStarter(mEventBus);
    }

    public EventBus getEventBus() {
        return mEventBus;
    }
}
