package fi.iki.asb.maha2.event;

import android.view.SurfaceHolder;

public class SurfaceChangedEvent {

    private final SurfaceHolder mSurfaceHolder;

    public SurfaceChangedEvent(SurfaceHolder surfaceHolder) {
        mSurfaceHolder = surfaceHolder;
    }

    public SurfaceHolder getSurfaceHolder() {
        return mSurfaceHolder;
    }

    @Override
    public String toString() {
        return "SurfaceChangedEvent [" +
                "mSurfaceHolder=" + mSurfaceHolder +
                ']';
    }
}
