package fi.iki.asb.maha2.ui.controller;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.common.eventbus.Subscribe;

import fi.iki.asb.maha.Constants;
import fi.iki.asb.maha.event.ColorSelectionUpdatedEvent;
import fi.iki.asb.maha.event.ModelUpdatedEvent;
import fi.iki.asb.maha.event.StartNewGameEvent;
import fi.iki.asb.maha.model.Color;
import fi.iki.asb.maha.model.Location;
import fi.iki.asb.maha.model.Model;
import fi.iki.asb.maha.model.UpdatedLocation;
import fi.iki.asb.maha2.BuildConfig;
import fi.iki.asb.maha2.R;
import fi.iki.asb.maha2.ui.VisualModelCallback;
import fi.iki.asb.maha2.ui.paintable.Block;

public class VisualModelBlockController {

    private static final String TAG = "VisualModelBlockControl";

    /**
     * Colors available.
     */
    private final int[] mColors;

    /**
     * The blocks.
     */
    private final Block[] mBlocks;

    /**
     * The callback for controlling the model.
     */
    private final VisualModelCallback mCallback;

    // =================================================================== //

    public VisualModelBlockController(
            final Context context,
            final VisualModelCallback callback) {

        mColors = new int[5];
        mColors[0] = ContextCompat.getColor(context, R.color.colorBlock0);
        mColors[1] = ContextCompat.getColor(context, R.color.colorBlock1);
        mColors[2] = ContextCompat.getColor(context, R.color.colorBlock2);
        mColors[3] = ContextCompat.getColor(context, R.color.colorBlock3);

        mBlocks = new Block[Constants.SIZE.size()];
        mCallback = callback;
    }

    @Subscribe
    public synchronized void onStartNewGame(StartNewGameEvent event) {
        for (int i = 0; i < mBlocks.length; i++) {
            if (mBlocks[i] != null) {

                /*
                if (BuildConfig.DEBUG)
                    Log.d(TAG, "onStartNewGame(): Removing " + mBlocks[i]
                            + " from " + event.getDimensions().location(i));
                 */

                mCallback.removePaintable(mBlocks[i]);
                mBlocks[i] = null;
            }
        }
    }

    @Subscribe
    public synchronized void onModelUpdated(ModelUpdatedEvent event) {
        final Model model = event.getModel();

        // Remove old blocks.
        for (UpdatedLocation ul: event.getLocations()) {
            if (ul.getOldLocation() != null) {
                final int index = Constants.SIZE.index(ul.getOldLocation());
                Block block = mBlocks[index];
                mBlocks[index] = null;

                /*
                if (BuildConfig.DEBUG)
                    Log.d(TAG, "onModelUpdated(): Removing " + block
                           + " from " + ul.getOldLocation());
                 */

                if (block != null)
                    mCallback.removePaintable(block);
            }
        }

        // Add new blocks.
        for (UpdatedLocation ul: event.getLocations()) {
            if (ul.getNewLocation() != null) {
                final Color c = model.getColorAt(ul.getNewLocation());
                if (c == null)
                    continue;

                final Block block = new Block(
                        ul.getNewLocation().getColumn(),
                        ul.getNewLocation().getRow(),
                        mColors[c.getValue()]);

                /*
                if (BuildConfig.DEBUG)
                    Log.d(TAG, "onModelUpdated(): Adding " + block
                            + " to " + ul.getNewLocation());
                 */

                mBlocks[Constants.SIZE.index(ul.getNewLocation())] = block;
                mCallback.addPaintable(block);
            }
        }

        // Make joined blocks look continuous.
        for (Location l: model.getDimensions().forEach()) {
            final Color c = model.getColorAt(l);
            if (c == null)
                continue;

            final Block block = mBlocks[Constants.SIZE.index(l)];

            block.setJoinedU(c.equals(model.getColorAt(l.up())));
            block.setJoinedD(c.equals(model.getColorAt(l.down())));
            block.setJoinedR(c.equals(model.getColorAt(l.right())));
            block.setJoinedL(c.equals(model.getColorAt(l.left())));
        }

        mCallback.flagChange();
    }

    @Subscribe
    public synchronized void onColorSelectionUpdated(ColorSelectionUpdatedEvent event) {
        for (int i = 0; i < mBlocks.length; i++)
            if (mBlocks[i] != null)
                mBlocks[i].setSelected(false);

        for (Location l: event.getSelection().getLocations())
            mBlocks[Constants.SIZE.index(l)].setSelected(true);

        mCallback.flagChange();
    }

}
