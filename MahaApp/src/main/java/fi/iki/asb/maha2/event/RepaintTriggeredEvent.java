package fi.iki.asb.maha2.event;

import fi.iki.asb.maha2.ui.RepaintPendingFlag;

public class RepaintTriggeredEvent {

    private final long mPaintTime;

    private final RepaintPendingFlag mPendingFlag;

    public RepaintTriggeredEvent(final long paintTime,
                                 final RepaintPendingFlag pendingFlag) {
        mPaintTime = paintTime;
        mPendingFlag = pendingFlag;
    }

    // =================================================================== //

    public long getPaintTime() {
        return mPaintTime;
    }

    public RepaintPendingFlag getPendingFlag() {
        return mPendingFlag;
    }

    // =================================================================== //

    @Override
    public String toString() {
        return "RepaintTriggeredEvent{" +
                "mPaintTime=" + mPaintTime +
                ", mPendingFlag=" + mPendingFlag +
                '}';
    }
}
