package fi.iki.asb.maha2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.widget.TextView;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import fi.iki.asb.maha.event.ScoredUpdatedEvent;
import fi.iki.asb.maha2.event.RestoreGameStateEvent;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    /**
     * The desired frame rate.
     */
    private static final long FRAME_RATE = 25;

    // ======================================================================================== //

    private RepaintTrigger mRepaintTrigger;

    private Thread mPainterThread;

    // ======================================================================================== //

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate()");

        setContentView(R.layout.activity_main_2);

        SurfaceView surfaceView = (SurfaceView) findViewById(R.id.SurfaceView);
        surfaceView.getHolder().addCallback(new ViewportChangeListener(getBus()));
        surfaceView.setOnTouchListener(new ViewportTouchListener(getBus()));

        mRepaintTrigger = new RepaintTrigger(getBus(), 1000L / FRAME_RATE);

        getBus().register(this);
        getBus().post(new RestoreGameStateEvent());
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.d(TAG, "onResume()");

        synchronized (this) {
            mPainterThread = new Thread(mRepaintTrigger);
            mPainterThread.start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.d(TAG, "onPause()");

        synchronized (this) {
            if (mPainterThread != null) {
                mRepaintTrigger.pause();
                mPainterThread = null;
            }
        }
    }

    private EventBus getBus() {
        return ((MahaApp) getApplication()).getEventBus();
    }
}
