package fi.iki.asb.maha2.ui;

import android.content.Context;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import fi.iki.asb.maha.event.StartNewGameEvent;
import fi.iki.asb.maha2.event.RepaintEvent;
import fi.iki.asb.maha2.event.RepaintTriggeredEvent;
import fi.iki.asb.maha2.event.SurfaceChangedEvent;
import fi.iki.asb.maha2.ui.controller.GameOverController;
import fi.iki.asb.maha2.ui.controller.ScoreTextController;
import fi.iki.asb.maha2.ui.controller.VisualModelBackgroundController;
import fi.iki.asb.maha2.ui.controller.VisualModelBlockController;

/**
 * Class that contains the elements for the visual representation of the game.
 */
public final class VisualModel implements VisualModelCallback {

    private static final String TAG = "VisualModel";

    private final EventBus mEventBus;

    /**
     * The paintables in this model.
     */
    private final List<Paintable> mPaintables;

    /**
     * The commands in this model.
     */
    private final List<Command> mCommands;

    /**
     * Copy of the commands in this model.
     */
    private final List<Command> mCommandsCopy;

    /**
     * Has the visual model mChanged since last RepaintEvent?
     */
    private boolean mChanged = false;

    // =================================================================== //

    public VisualModel(Context context, EventBus eventBus) {
        mEventBus = eventBus;
        mPaintables = new ArrayList<Paintable>();
        mCommands = new ArrayList<Command>();
        mCommandsCopy = new ArrayList<Command>();

        // The order in which the components are registered to the bus is
        // important.
        mEventBus.register(this);
        mEventBus.register(new GameOverController(context, this));
        mEventBus.register(new VisualModelBlockController(context, this));
        mEventBus.register(new VisualModelBackgroundController(context, this));
        mEventBus.register(new ScoreTextController(context, this));
    }

    // =================================================================== //

    @Override
    public synchronized void flagChange() {
        mChanged = true;
    }

    @Override
    public synchronized void addPaintable(Paintable paintable) {
        flagChange();
        mPaintables.add(paintable);
    }

    @Override
    public synchronized boolean removePaintable(Paintable paintable) {
        flagChange();
        return mPaintables.remove(paintable);
    }

    @Override
    public synchronized void addCommand(Command command) {
        mCommands.add(command);
    }

    @Override
    public synchronized void removeCommand(Command command) {
        mCommands.remove(command);
    }

    // =================================================================== //

    /**
     * Force redraw when the UI component changes (e.g. app is resumed).
     */
    @Subscribe
    public synchronized void onStartNewGame(StartNewGameEvent event) {
        mCommands.clear();
        mPaintables.clear();
        flagChange();
    }

    /**
     * Force redraw when the UI component changes (e.g. app is resumed).
     */
    @Subscribe
    public synchronized void onSurfaceChanged(SurfaceChangedEvent event) {
        // TODO: As the surface size may have changed, probably should also
        // clear the model from paintables and commands and let the
        // controllers put them back in proper size.
        flagChange();
    }

    int counter = 0;

    /**
     * Updates visual model to represent the time in the event and posts a
     * RepaintEvent.
     */
    @Subscribe
    public synchronized void onRepaintTriggered(RepaintTriggeredEvent event) {

        // if (BuildConfig.DEBUG)
        //     Log.d(TAG, "onRepaintTriggered(): mCommands.size()=" + mCommands.size());

        // Make a copy so that modifications can be made to the original in
        // add/removeCommand.
        mCommandsCopy.addAll(mCommands);
        for (Command command: mCommandsCopy)
            command.execute(event.getPaintTime());
        mCommandsCopy.clear();

        // if (BuildConfig.DEBUG)
        //     Log.d(TAG, "onRepaintTriggered(): mPaintables.size()=" + mPaintables.size());

        if (mChanged) {
            mChanged = false;
            mEventBus.post(new RepaintEvent(new ArrayList<>(mPaintables)));
        }

        event.getPendingFlag().setRepaintPending(false);
    }
}
