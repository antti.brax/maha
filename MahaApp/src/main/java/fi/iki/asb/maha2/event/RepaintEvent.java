package fi.iki.asb.maha2.event;

import java.util.List;

import fi.iki.asb.maha2.ui.Paintable;

public class RepaintEvent {

    private final List<Paintable> mPaintables;

    public RepaintEvent(List<Paintable> paintables) {
        this.mPaintables = paintables;
    }

    public List<Paintable> getPaintables() {
        return mPaintables;
    }

    @Override
    public String toString() {
        return "RepaintEvent [mPaintables=" + mPaintables + "]";
    }

}
