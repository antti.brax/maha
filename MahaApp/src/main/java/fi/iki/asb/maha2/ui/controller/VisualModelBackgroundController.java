package fi.iki.asb.maha2.ui.controller;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import com.google.common.eventbus.Subscribe;

import fi.iki.asb.maha.event.GameOverEvent;
import fi.iki.asb.maha.event.StartNewGameEvent;
import fi.iki.asb.maha2.R;
import fi.iki.asb.maha2.event.SurfaceChangedEvent;
import fi.iki.asb.maha2.ui.VisualModelCallback;
import fi.iki.asb.maha2.ui.paintable.Background;

public class VisualModelBackgroundController {

    private final VisualModelCallback mCallback;

    private final Background mBackground;

    public VisualModelBackgroundController(Context context, VisualModelCallback callback) {
        mCallback = callback;
        mBackground = new Background(
                ContextCompat.getColor(context, R.color.colorPrimaryDark));
    }

    @Subscribe
    public synchronized void onStartNewGame(StartNewGameEvent event) {
        mCallback.removePaintable(mBackground);
        mCallback.addPaintable(mBackground);
    }

    @Subscribe
    public synchronized void onSurfaceChanged(SurfaceChangedEvent event) {
        mCallback.removePaintable(mBackground);
        mCallback.addPaintable(mBackground);
    }
}
