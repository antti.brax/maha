package fi.iki.asb.maha2.ui.paintable;

import android.graphics.*;

import fi.iki.asb.maha2.ui.Glyph;
import fi.iki.asb.maha2.ui.Paintable;

import java.util.HashMap;
import java.util.Map;

public class Text implements Paintable {

    /**
     * Each character consists of one or more rectangle. When drawn
     * with EVEN_ODD fill, they form the glyph.
     */
    private static final Map<Character, Glyph> GLYPHS = new HashMap<>();

    static {
        GLYPHS.put('0', new Glyph(3)
                .add(new RectF(0, 0, 3, 5))
                .add(new RectF(1, 1, 2, 4))
        );
        GLYPHS.put('1', new Glyph(1)
                .add(new RectF(0, 0, 1, 5))
        );
        GLYPHS.put('2', new Glyph(3)
                .add(new RectF(0, 0, 3, 5))
                .add(new RectF(0, 1, 2, 2))
                .add(new RectF(1, 3, 3, 4))
        );
        GLYPHS.put('3', new Glyph(3)
                .add(new RectF(0, 0, 3, 5))
                .add(new RectF(0, 1, 2, 2))
                .add(new RectF(0, 3, 2, 4))
        );
        GLYPHS.put('4', new Glyph(3)
                .add(new RectF(0, 0, 3, 5))
                .add(new RectF(1, 0, 2, 2))
                .add(new RectF(0, 3, 2, 5))
        );
        GLYPHS.put('5', new Glyph(3)
                .add(new RectF(0, 0, 3, 5))
                .add(new RectF(1, 1, 3, 2))
                .add(new RectF(0, 3, 2, 4))
        );
        GLYPHS.put('6', new Glyph(3)
                .add(new RectF(0, 0, 3, 5))
                .add(new RectF(1, 1, 3, 2))
                .add(new RectF(1, 3, 2, 4))
        );
        GLYPHS.put('7', new Glyph(3)
                .add(new RectF(0, 0, 3, 5))
                .add(new RectF(0, 1, 2, 5))
        );
        GLYPHS.put('8', new Glyph(3)
                .add(new RectF(0, 0, 3, 5))
                .add(new RectF(1, 1, 2, 2))
                .add(new RectF(1, 3, 2, 4))
        );
        GLYPHS.put('9', new Glyph(3)
                .add(new RectF(0, 0, 3, 5))
                .add(new RectF(1, 1, 2, 2))
                .add(new RectF(0, 3, 2, 4))
        );
        GLYPHS.put(' ', new Glyph(2));
        GLYPHS.put('.', new Glyph(1)
                .add(new RectF(0, 4, 1, 5))
        );
        GLYPHS.put(':', new Glyph(1)
                .add(new RectF(0, 1, 1, 2))
                .add(new RectF(0, 3, 1, 4))
        );
        GLYPHS.put('A', new Glyph(3)
                .add(new RectF(0, 0, 3, 5))
                .add(new RectF(1, 1, 2, 2))
                .add(new RectF(1, 3, 2, 5))
        );
        GLYPHS.put('B', new Glyph(3)
                .add(new RectF(0, 0, 3, 5))
                .add(new RectF(1, 1, 2, 2))
                .add(new RectF(1, 3, 2, 4))
                .add(new RectF(2, 0, 3, 1))
        );
        GLYPHS.put('C', new Glyph(3)
                .add(new RectF(0, 0, 3, 5))
                .add(new RectF(1, 1, 3, 4))
        );
        GLYPHS.put('D', new Glyph(3)
                .add(new RectF(0, 0, 3, 5))
                .add(new RectF(1, 1, 2, 4))
                .add(new RectF(2, 0, 3, 1))
        );
        GLYPHS.put('E', new Glyph(3)
                .add(new RectF(0, 0, 3, 5))
                .add(new RectF(1, 1, 3, 2))
                .add(new RectF(1, 3, 3, 4))
        );
        GLYPHS.put('F', new Glyph(3)
                .add(new RectF(0, 0, 3, 5))
                .add(new RectF(1, 1, 3, 2))
                .add(new RectF(1, 3, 3, 5))
        );
        GLYPHS.put('G', new Glyph(3)
                .add(new RectF(0, 0, 3, 5))
                .add(new RectF(1, 1, 2, 4))
                .add(new RectF(2, 1, 3, 2))
        );
        GLYPHS.put('H', new Glyph(3)
                .add(new RectF(0, 0, 3, 5))
                .add(new RectF(1, 0, 2, 2))
                .add(new RectF(1, 3, 2, 5))
        );
        GLYPHS.put('I', new Glyph(1)
                .add(new RectF(0, 0, 1, 5))
        );
        GLYPHS.put('J', new Glyph(3)
                .add(new RectF(0, 0, 3, 5))
                .add(new RectF(0, 0, 2, 3))
                .add(new RectF(1, 3, 2, 4))
        );
        GLYPHS.put('K', new Glyph(3)
                .add(new RectF(0, 0, 3, 5))
                .add(new RectF(1, 0, 2, 2))
                .add(new RectF(1, 3, 2, 5))
                .add(new RectF(2, 2, 3, 3))
        );
        GLYPHS.put('L', new Glyph(3)
                .add(new RectF(0, 0, 3, 5))
                .add(new RectF(1, 0, 3, 4))
        );
        GLYPHS.put('M', new Glyph(5)
                .add(new RectF(0, 0, 5, 5))
                .add(new RectF(1, 0, 4, 1))
                .add(new RectF(2, 1, 3, 2))
                .add(new RectF(1, 2, 2, 3))
                .add(new RectF(3, 2, 4, 3))
                .add(new RectF(1, 3, 4, 5))
        );
        GLYPHS.put('N', new Glyph(4)
                .add(new RectF(0, 0, 4, 5))
                .add(new RectF(1, 0, 3, 1))
                .add(new RectF(2, 1, 3, 2))
                .add(new RectF(1, 2, 2, 3))
                .add(new RectF(1, 3, 3, 5))
        );
        GLYPHS.put('O', new Glyph(3)
                .add(new RectF(0, 0, 3, 5))
                .add(new RectF(1, 1, 2, 4))
        );
        GLYPHS.put('P', new Glyph(3)
                .add(new RectF(0, 0, 3, 5))
                .add(new RectF(1, 1, 2, 2))
                .add(new RectF(1, 3, 3, 5))
        );
        GLYPHS.put('Q', new Glyph(3)
                .add(new RectF(0, 0, 3, 5))
                .add(new RectF(1, 1, 2, 3))
        );
        GLYPHS.put('R', new Glyph(3)
                .add(new RectF(0, 0, 3, 5))
                .add(new RectF(1, 1, 2, 2))
                .add(new RectF(2, 2, 3, 3))
                .add(new RectF(1, 3, 2, 5))
        );
        GLYPHS.put('S', new Glyph(3)
                .add(new RectF(0, 0, 3, 5))
                .add(new RectF(1, 1, 3, 2))
                .add(new RectF(0, 3, 2, 4))
        );
        GLYPHS.put('T', new Glyph(3)
                .add(new RectF(0, 0, 3, 5))
                .add(new RectF(0, 1, 1, 5))
                .add(new RectF(2, 1, 3, 5))
        );
        GLYPHS.put('U', new Glyph(3)
                .add(new RectF(0, 0, 3, 5))
                .add(new RectF(1, 0, 2, 4))
        );
        GLYPHS.put('V', new Glyph(3)
                .add(new RectF(0, 0, 3, 5))
                .add(new RectF(1, 0, 2, 4))
                .add(new RectF(0, 4, 1, 5))
                .add(new RectF(2, 4, 3, 5))
        );
        GLYPHS.put('W', new Glyph(5)
                .add(new RectF(0, 0, 5, 5))
                .add(new RectF(1, 0, 4, 2))
                .add(new RectF(1, 2, 2, 3))
                .add(new RectF(3, 2, 4, 3))
                .add(new RectF(2, 3, 3, 4))
                .add(new RectF(1, 4, 4, 5))

        );
        GLYPHS.put('X', new Glyph(3)
                .add(new RectF(0, 0, 3, 5))
                .add(new RectF(1, 0, 2, 2))
                .add(new RectF(1, 3, 2, 5))
                .add(new RectF(0, 2, 1, 3))
                .add(new RectF(2, 2, 3, 3))
        );
        GLYPHS.put('Y', new Glyph(3)
                .add(new RectF(0, 0, 3, 5))
                .add(new RectF(1, 0, 2, 2))
                .add(new RectF(0, 2, 1, 5))
                .add(new RectF(2, 2, 3, 5))
        );
        GLYPHS.put('Z', new Glyph(3)
                .add(new RectF(0, 0, 3, 5))
                .add(new RectF(0, 1, 2, 2))
                .add(new RectF(0, 2, 1, 3))
                .add(new RectF(2, 2, 3, 3))
                .add(new RectF(1, 3, 3, 4))
        );
    }

    // =================================================================== //

    private Paint mPaint;

    private float mTextSize = 1.0f;

    private float mX = 0.0f;

    private float mY = 0.0f;

    private int mLayerHeight = 0;

    private String mText = "";

    // =================================================================== //

    public Text(String text, Paint paint) {
        mText = text;
        mPaint = paint;
    }

    // =================================================================== //

    public void setText(String text) {
        mText = text;
    }

    @Override
    public int getLayerHeight() {
        return mLayerHeight;
    }

    public void setLayerHeight(int layerHeight) {
        mLayerHeight = layerHeight;
    }

    public void setX(float x) {
        mX = x;
    }

    public void setY(float y) {
        mY = y;
    }

    public RectF getBounds(RectF bounds) {
        bounds.set(0.0f, 0.0f, 0.0f, 0.0f);

        for (int i = 0; i < mText.length(); i++) {
            Glyph glyph = GLYPHS.get(mText.charAt(i));
            if (glyph == null)
                continue;

            if (i > 0)
                bounds.right += 1.0f;

            bounds.right += glyph.getWidth();
            bounds.bottom = 5;
        }

        bounds.right *= mTextSize / 5.0f;
        bounds.bottom *= mTextSize / 5.0f;

        return bounds;
    }

    /**
     * Set text size in pixels.
     */
    public void setTextSize(float textSizePixels) {
        mTextSize = textSizePixels;
    }

    @Override
    public void drawOn(Canvas canvas) {
        canvas.save();
        canvas.translate(mX, mY);
        canvas.scale(mTextSize / 5.0f, mTextSize / 5.0f);

        for (int i = 0; i < mText.length(); i++) {
            Glyph glyph = GLYPHS.get(mText.charAt(i));
            if (glyph == null)
                continue;

            canvas.drawPath(glyph.getPath(), mPaint);
            canvas.translate(glyph.getWidth() + 1.0f, 0.0f);
        }

        canvas.restore();
    }
}
