package fi.iki.asb.maha2.ui.paintable;

import android.graphics.Canvas;

import fi.iki.asb.maha2.ui.Paintable;

/**
 * Paintable used for clearing the background. Might do something fancier later.
 */
public class Background implements Paintable {

    private final int mColor;

    public Background(int color) {
        mColor = color;
    }

    @Override
    public int getLayerHeight() {
        return -100;
    }

    @Override
    public void drawOn(Canvas canvas) {
        canvas.drawColor(mColor);
    }
}
