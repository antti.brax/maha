package fi.iki.asb.maha2.ui;

/**
 * A flag that is raised if a repaint is pending and lowered when a repaint
 * has been performed. This is used to prevent the RepaintTrigger from
 * flooding the eventBus with RepaintTriggeredEvents in the case when the
 * repainting takes longer than the delay between frame updates.
 */
public class RepaintPendingFlag {

    private boolean mRepaintPending = false;

    public synchronized void setRepaintPending(boolean repaintPending) {
        mRepaintPending = repaintPending;
    }

    public synchronized boolean isRepaintPending() {
        return mRepaintPending;
    }

    @Override
    public synchronized String toString() {
        return "RepaintPendingFlag{" +
                "mRepaintPending=" + mRepaintPending +
                '}';
    }
}
