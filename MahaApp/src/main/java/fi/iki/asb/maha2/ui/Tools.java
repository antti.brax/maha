package fi.iki.asb.maha2.ui;

import android.content.Context;
import android.util.DisplayMetrics;

public class Tools {

    public static float dpToPx(Context context, float dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT);
    }

}
