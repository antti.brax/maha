package fi.iki.asb.maha2.ui;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import fi.iki.asb.maha2.event.RepaintEvent;
import fi.iki.asb.maha2.event.SurfaceChangedEvent;

/**
 * Class that performs the actual painting.
 */
public class Painter {

    private final EventBus mEventBus;

    private SurfaceHolder mSurfaceHolder;

    public Painter(EventBus eventBus) {
        mEventBus = eventBus;
        mEventBus.register(this);
    }

    @Subscribe
    public synchronized void onSurfaceChanged(SurfaceChangedEvent event) {
        mSurfaceHolder = event.getSurfaceHolder();
    }

    @Subscribe
    public synchronized void onRepaint(RepaintEvent event) {
        if (mSurfaceHolder == null)
            return;

        final Canvas canvas = mSurfaceHolder.lockCanvas();
        if (canvas != null) {
            int count = canvas.save();

            final List<Paintable> paintables = event.getPaintables();
            Collections.sort(paintables, new Comparator<Paintable>() {
                @Override
                public int compare(Paintable lhs, Paintable rhs) {
                    return lhs.getLayerHeight() - rhs.getLayerHeight();
                }
            });

            for (Paintable p: paintables)
                p.drawOn(canvas);

            canvas.restoreToCount(count);
            mSurfaceHolder.unlockCanvasAndPost(canvas);
        }
    }

}
