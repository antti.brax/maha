package fi.iki.asb.maha2;

import android.content.SharedPreferences;
import android.util.Base64;
import android.util.Log;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;

import fi.iki.asb.maha.Constants;
import fi.iki.asb.maha.event.ModelUpdatedEvent;
import fi.iki.asb.maha.event.ScoredUpdatedEvent;
import fi.iki.asb.maha.event.StartNewGameEvent;
import fi.iki.asb.maha.event.StatisticsUpdatedEvent;
import fi.iki.asb.maha.logic.ModelUpdater;
import fi.iki.asb.maha.model.*;
import fi.iki.asb.maha2.event.RestoreGameStateEvent;

/**
 * Class that saves game state.
 */
public class GameStateSaver {

    private static final String TAG = "GameStateSaver";
    private static final String KEY_GAME_MODEL = "game.model";
    private static final String KEY_GAME_SCORE = "game.score";
    private static final String KEY_GAME_STATISTICS = "game.statistics";

    private final EventBus mEventBus;

    private final SharedPreferences mStorage;

    public GameStateSaver(EventBus eventBus, SharedPreferences storage) {
        mStorage = storage;
        mEventBus = eventBus;
        mEventBus.register(this);
    }

    @Subscribe
    public synchronized void onStatisticsUpdated(StatisticsUpdatedEvent event) {

        if (BuildConfig.DEBUG)
            Log.d(TAG, "onStatisticsUpdated(): " + event);

        try {
            ByteArrayOutputStream bo = new ByteArrayOutputStream(64);
            ObjectOutputStream oo = new ObjectOutputStream(bo);
            Statistics.write(event.getStatistics(), oo);
            oo.flush();

            mStorage.edit()
                    .putString(KEY_GAME_STATISTICS,
                            Base64.encodeToString(bo.toByteArray(),
                                    Base64.DEFAULT))
                    .apply();
        } catch (IOException ex) {
            // TODO: Error handling.
        }
    }

    @Subscribe
    public synchronized void onModelUpdated(ModelUpdatedEvent event) {

        if (BuildConfig.DEBUG)
            Log.d(TAG, "onModelUpdated(): " + event);

        try {
            ByteArrayOutputStream bo = new ByteArrayOutputStream(64);
            ObjectOutputStream oo = new ObjectOutputStream(bo);
            Model.write(event.getModel(), oo);
            oo.flush();

            mStorage.edit()
                    .putString(KEY_GAME_MODEL,
                            Base64.encodeToString(bo.toByteArray(),
                                    Base64.DEFAULT))
                    .apply();
        } catch (IOException ex) {
            // TODO: Error handling.
        }
    }

    @Subscribe
    public synchronized void onScoreUpdated(ScoredUpdatedEvent event) {

        if (BuildConfig.DEBUG)
        Log.d(TAG, "onScoreUpdated(): " + event);

        try {
            ByteArrayOutputStream bo = new ByteArrayOutputStream(64);
            ObjectOutputStream oo = new ObjectOutputStream(bo);
            Score.write(event.getScore(), oo);
            oo.flush();

            mStorage.edit()
                    .putString(KEY_GAME_SCORE,
                            Base64.encodeToString(bo.toByteArray(),
                                    Base64.DEFAULT))
                    .apply();
        } catch (IOException ex) {
            // TODO: Error handling.
        }
    }

    @Subscribe
    public synchronized void onRestoreGameState(RestoreGameStateEvent event) {
        final Score score = restoreScore();
        final Statistics stats = restoreStatistics();
        final ModelUpdatedEvent modelUpdatedEvent = restoreModel();

        if (score == null || stats == null || modelUpdatedEvent == null){
            mEventBus.post(new StartNewGameEvent(Constants.SIZE));
        } else{
            mEventBus.post(new StatisticsUpdatedEvent(stats));
            mEventBus.post(new ScoredUpdatedEvent(score));
            mEventBus.post(modelUpdatedEvent);
        }
    }

    private Score restoreScore() {
        final String str = mStorage.getString(KEY_GAME_SCORE, null);

        if (str == null)
            return null;

        try {
            ByteArrayInputStream bi = new ByteArrayInputStream(Base64.decode(str,
                    Base64.DEFAULT));
            ObjectInputStream oi = new ObjectInputStream(bi);
            return Score.read(oi);
        } catch (IOException ex) {
            // TODO: Error handling.
            return null;
        }
    }

    private Statistics restoreStatistics() {
        final String str = mStorage.getString(KEY_GAME_STATISTICS, null);

        if (str == null)
            return null;

        try {
            ByteArrayInputStream bi = new ByteArrayInputStream(Base64.decode(str,
                    Base64.DEFAULT));
            ObjectInputStream oi = new ObjectInputStream(bi);
            return Statistics.read(oi);
        } catch (IOException ex) {
            // TODO: Error handling.
            return null;
        }
    }

    private ModelUpdatedEvent restoreModel() {
        final String str = mStorage.getString(KEY_GAME_MODEL, null);

        if (str == null)
            return null;

        try {
            ByteArrayInputStream bi = new ByteArrayInputStream(Base64.decode(str,
                    Base64.DEFAULT));
            ObjectInputStream oi = new ObjectInputStream(bi);
            Model model = Model.read(oi);

            Collection<UpdatedLocation> co = new ArrayList<>();
            for (Location l: model.getDimensions().forEach())
                if (model.getColorAt(l) != null)
                    co.add(new UpdatedLocation(null, l));

            return new ModelUpdatedEvent(model, co);
        } catch (IOException ex) {
            // TODO: Error handling.
            return null;
        }
    }
}
