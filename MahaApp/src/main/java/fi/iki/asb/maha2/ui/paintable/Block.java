package fi.iki.asb.maha2.ui.paintable;

import android.graphics.Canvas;
import android.graphics.Paint;

import fi.iki.asb.maha.Constants;
import fi.iki.asb.maha2.ui.Paintable;

/**
 * A block as shown on screen.
 */
public class Block implements Paintable {

    private static Paint WHITE;
    private static Paint BLACK;

    {
        WHITE = new Paint();
        WHITE.setARGB(0x30, 0xFF, 0xFF, 0xFF);
        WHITE.setStyle(Paint.Style.FILL);

        BLACK = new Paint();
        BLACK.setARGB(0x30, 0x00, 0x00, 0x00);
        BLACK.setStyle(Paint.Style.FILL);
    }

    private final Paint mPaint;

    private float mColumn;
    private float mRow;
    private boolean mSelected = false;
    private boolean joinedL = false;
    private boolean joinedR = false;
    private boolean joinedU = false;
    private boolean joinedD = false;

    public Block(float column, float row, int color) {
        mColumn = column;
        mRow = row;
        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setColor(color);
    }

    public void setColumn(float column) {
        mColumn = column;
    }

    public void setRow(float row) {
        mRow = row;
    }

    public void setJoinedL(boolean joinedL) {
        this.joinedL = joinedL;
    }

    public void setJoinedR(boolean joinedR) {
        this.joinedR = joinedR;
    }

    public void setJoinedU(boolean joinedU) {
        this.joinedU = joinedU;
    }

    public void setJoinedD(boolean joinedD) {
        this.joinedD = joinedD;
    }

    public void setSelected(boolean selected) {
        mSelected = selected;
    }

    @Override
    public int getLayerHeight() {
        return 0;
    }

    @Override
    public void drawOn(Canvas canvas) {
        canvas.save();

        final float scale = canvas.getWidth() / (float) Constants.SIZE.getWidth();
        canvas.scale(scale, scale);
        canvas.translate(mColumn, mRow);

        float l = joinedL ? 0.0f : 0.015f;
        float u = joinedU ? 0.0f : 0.015f;
        float d = joinedD ? 1.0f : 0.985f;
        float r = joinedR ? 1.0f : 0.985f;
        canvas.drawRect(l, u, r, d, mPaint);

        if (mSelected) {
            canvas.drawRect(l, u, r, d, BLACK);
        } else {
            if (! joinedU)
                canvas.drawRect(l, u, r, 0.075f, WHITE);
            if (! joinedL)
                canvas.drawRect(l, u, 0.075f, d, WHITE);
            if (! joinedD)
                canvas.drawRect(l, 0.925f, r, d, BLACK);
            if (! joinedR)
                canvas.drawRect(0.925f, u, r, d, BLACK);
        }

        canvas.restore();
    }
}
