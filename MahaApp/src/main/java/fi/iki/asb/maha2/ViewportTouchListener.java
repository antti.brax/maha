package fi.iki.asb.maha2;

import android.content.res.Resources;
import android.view.MotionEvent;
import android.view.View;

import com.google.common.eventbus.EventBus;

import fi.iki.asb.maha.event.ViewportTouchEvent;
import fi.iki.asb.maha.model.Location;

/**
 * Post a ViewportTouchEvent when ACTION_UP is detected and the user has not dragged the pointer
 * too far.
 */
public class ViewportTouchListener implements View.OnTouchListener {

    /**
     * Screen DPI.
     */
    private static final float DPI = Resources.getSystem().getDisplayMetrics().densityDpi;

    /**
     * If the user drags the pointer father than this, the touch event is ignored.
     */
    private static final float DRAG_LIMIT_MM = 5.0f;

    // ======================================================================================== //

    private final EventBus mEventBus;

    private Location mPointerDown;

    // ======================================================================================== //

    public ViewportTouchListener(EventBus eventBus) {
        mEventBus = eventBus;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            mPointerDown = Location.valueOf(
                    (int) event.getX(),
                    (int) event.getY());
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            final Location pointerUp = Location.valueOf(
                    (int) event.getX(),
                    (int) event.getY());
            final float dist = pointerUp.distanceTo(mPointerDown) / DPI * 25.4f;
            mPointerDown = null;

            if (dist < DRAG_LIMIT_MM)
                mEventBus.post(new ViewportTouchEvent(pointerUp));
        }

        return true;
    }
}
