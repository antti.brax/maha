package fi.iki.asb.maha2.ui.paintable;

import android.graphics.Canvas;

import fi.iki.asb.maha2.ui.Paintable;

public class GameOver implements Paintable {

    private final int mColor;

    public GameOver(int color) {
        mColor = color;
    }

    @Override
    public int getLayerHeight() {
        return 200;
    }

    @Override
    public void drawOn(Canvas canvas) {
        canvas.drawColor(mColor);
    }
}
