package fi.iki.asb.maha2;

import android.util.Log;

import com.google.common.base.Stopwatch;
import com.google.common.eventbus.EventBus;

import java.util.concurrent.TimeUnit;

import fi.iki.asb.maha2.event.RepaintTriggeredEvent;
import fi.iki.asb.maha2.ui.RepaintPendingFlag;

class RepaintTrigger implements Runnable {

    private static final String TAG = "RepaintTrigger";

    private boolean mPauseRequested = false;

    private final EventBus mEventBus;

    private final Stopwatch mStopwatch;

    private final long mDelayMillis;

    private final RepaintPendingFlag mRepaintPendingFlag;

    // ======================================================================================== //

    RepaintTrigger(EventBus eventBus, long delayMillis) {

        if (BuildConfig.DEBUG)
            Log.d(TAG, "RepaintTrigger(...)");

        mEventBus = eventBus;
        mStopwatch = Stopwatch.createUnstarted();
        mDelayMillis = delayMillis;
        mRepaintPendingFlag = new RepaintPendingFlag();
    }

    // ======================================================================================== //

    synchronized void pause() {

        if (BuildConfig.DEBUG)
            Log.d(TAG, "pause()");

        if (!mPauseRequested) {
            mStopwatch.stop();
            mPauseRequested = true;
        }
        notifyAll();
    }

    // ======================================================================================== //

    @Override
    public synchronized void run() {

        if (BuildConfig.DEBUG)
            Log.d(TAG, "run()");

        mStopwatch.start();
        mPauseRequested = false;
        while (!mPauseRequested) {
            final long currentTime = mStopwatch.elapsed(TimeUnit.MILLISECONDS);

            // Only post a new repaint event if the previous has been processed.
            if (! mRepaintPendingFlag.isRepaintPending()) {
                mRepaintPendingFlag.setRepaintPending(true);
                mEventBus.post(new RepaintTriggeredEvent(currentTime, mRepaintPendingFlag));
            } else {
                Log.i(TAG, "Repaint is taking too long. Frame dropped at " + currentTime);
            }

            final long sleep = mDelayMillis - (currentTime % mDelayMillis);

            // if (BuildConfig.DEBUG)
            //     Log.d(TAG, "run(): Repaint at " + currentTime + ", sleep for " + sleep);

            try {
                wait(sleep);
            } catch (InterruptedException ex) {
                // Ignore.
            }
        }
    }
}
