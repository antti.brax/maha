package fi.iki.asb.maha2.ui;

/**
 * Created by antti on 3/1/17.
 */

public interface VisualModelCallback {

    /**
     * Flag a change in the model so that redraw will be triggered.
     */
    void flagChange();

    /**
     * Add a paintable to the visual model.
     */
    void addPaintable(Paintable paintable);

    /**
     * Remove a paintable from he visual model.
     */
    boolean removePaintable(Paintable paintable);

    /**
     * Add a command to the visual model.
     */
    void addCommand(Command command);

    /**
     * Remove a command from he visual model.
     */
    void removeCommand(Command command);

}
