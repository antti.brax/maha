package fi.iki.asb.maha2;

import android.util.Log;

import com.google.common.eventbus.DeadEvent;
import com.google.common.eventbus.Subscribe;

class DeadEventListener {

    private static final String TAG = "DeadEventListener";

    @Subscribe
    public void onDeadEvent(DeadEvent event) {
        Log.d(TAG, "onDeadEvent(...): " + event.toString());
    }

}
