package fi.iki.asb.maha2.ui;

import android.graphics.Path;
import android.graphics.RectF;

public class Glyph {
    private final int mWidth;
    private final Path mPath;

    public Glyph(int width) {
        mWidth = width;
        // http://stackoverflow.com/questions/14197823/draw-path-with-hole-android
        mPath = new Path();
        mPath.setFillType(Path.FillType.EVEN_ODD);
    }

    public Glyph(int width, RectF[] shapes) {
        this(width);
        for (RectF rect: shapes)
            mPath.addRect(rect, Path.Direction.CW);
    }

    public Glyph add(RectF rect) {
        mPath.addRect(rect, Path.Direction.CW);
        return this;
    }

    public Glyph add(Path path) {
        mPath.addPath(path);
        return this;
    }

    public Glyph add(float[] coordinates) {
        final Path path = new Path();
        for (int i = 0; i < coordinates.length; i+=2) {
            if (path.isEmpty())
                path.moveTo(coordinates[i], coordinates[i + 1]);
            else
                path.lineTo(coordinates[i], coordinates[i + 1]);
        }
        path.close();
        return add(path);
    }

    public int getWidth() {
        return mWidth;
    }

    public Path getPath() {
        return mPath;
    }
}
